package com.jiyi.cradleheadcamera.thread;

import android.os.Handler;

/**
 * Created by IDesigner on 2017/6/1.
 */

public class TimeCountThread extends Thread {
    Handler handler;

    public TimeCountThread(Handler handler) {
        this.handler = handler;
    }

    boolean flag = true;

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public void run() {
        super.run();
        int time = 0;
        while (flag) {
            try {
                sleep(1000);
                time++;
                handler.sendEmptyMessage(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
