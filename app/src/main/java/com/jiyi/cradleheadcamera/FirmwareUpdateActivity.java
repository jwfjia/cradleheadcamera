package com.jiyi.cradleheadcamera;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.bean.MsgBody;
import com.jiyi.cradleheadcamera.service.BleService;
import com.jiyi.cradleheadcamera.util.Codeutil;
import com.jiyi.cradleheadcamera.util.FileUtil;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

public class FirmwareUpdateActivity extends AppCompatActivity {

    int num = 0;
    int delay = 0;
    BleService bleService;
    byte[] fileBytes, xBytes, yBytes, zBytes, testBytes;

    int unit1 = 128;
    int unit2 = 17;
    byte[] ok = {0x55, (byte) 0xAE, 0x03, 0x12, 0x10, 0x22};
    byte[] cmdX = {(byte) 0xAA, 0x5A, 0x03, (byte) 0xBB, 0x66, 0x13};
    byte[] cmdY = {(byte) 0xAA, 0x5A, 0x03, (byte) 0xBB, 0x66, 0x14};
    byte[] cmdZ = {(byte) 0xAA, 0x5A, 0x03, (byte) 0xBB, 0x66, 0x15};
    byte[] cmdSync = {(byte) 0xAA, 0x5A, 0x02, 0x21, 0x20};
    byte[] cmdErase = {(byte) 0xAA, 0x5A, 0x02, 0x23, 0x20};
    byte[] cmdBoot = {(byte) 0xAA, 0x5A, 0x02, 0x30, 0x20};
    byte[] re;
    int tryCount = 4;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (msg.arg1 == 0) {
                        Toast.makeText(getApplicationContext(), "升级失败", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "升级成功", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 1:
                    MsgBody msgBody = (MsgBody) msg.obj;
                    byte[] data2 = msgBody.getData();
                    re = bleService.startWriteAndResponse(data2);
                    Log.i("lbw", "===re:" + Codeutil.bytesToHexString(re));
                    msgBody.getLatch().countDown();
                    break;
                case 2:
                    MsgBody msgBody2 = (MsgBody) msg.obj;
                    byte[] data3 = msgBody2.getData();
                    bleService.startWrite4Update("98d076f0-d6de-11e6-bf26-cec0c932ce01", "98d076f1-d6de-11e6-bf26-cec0c932ce01", data3, msgBody2.getLatch());
                    break;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firmware_update);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        RxBus.get().register(this);
        RxBus.get().post(BleService.TAG_REQUEST_SERVICE, "");
        initData();

        findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        boolean b1 = sendData(cmdX, xBytes);
                        if (b1) {
                            boolean b2 = sendData(cmdY, yBytes);
                            if (b2) {
                                boolean b3 = sendData(cmdZ, zBytes);
                                if (b3) {
                                    //success
                                    handler.sendMessage(handler.obtainMessage(0, 1, 0));
                                } else {
                                    handler.sendMessage(handler.obtainMessage(0, 0, 0));
                                    return;
                                }
                            } else {
                                handler.sendMessage(handler.obtainMessage(0, 0, 0));
                                return;
                            }
                        } else {
                            handler.sendMessage(handler.obtainMessage(0, 0, 0));
                            return;
                        }
                    }
                }.start();
            }
        });

    }


    private boolean sendData(byte[] type, byte[] data) {
        int index = 0;
        int xPos = 0;
        int count = 0;
        CountDownLatch latch;
        MsgBody msgBody;


        while (true) {
            latch = new CountDownLatch(1);
            msgBody = new MsgBody(latch, type);
            handler.sendMessage(handler.obtainMessage(1, msgBody));
            try {
                latch.await();
                Thread.sleep(delay);
                if (!Arrays.equals(ok, re)) {
                    if (count == tryCount) {
                        Log.i("lbw", "update fail!");
                        return false;
                    }
                    count++;
                } else {
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        count = 0;
        while (true) {
            latch = new CountDownLatch(1);
            msgBody = new MsgBody(latch, cmdSync);
            handler.sendMessage(handler.obtainMessage(1, msgBody));
            try {
                latch.await();
                Thread.sleep(delay);
                if (!Arrays.equals(ok, re)) {
                    if (count == tryCount) {
                        Log.i("lbw", "update fail!");
                        return false;
                    }
                    count++;
                } else {
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        count = 0;
        while (true) {
            latch = new CountDownLatch(1);
            msgBody = new MsgBody(latch, cmdErase);
            handler.sendMessage(handler.obtainMessage(1, msgBody));
            try {
                latch.await();
                Thread.sleep(delay);
                if (!Arrays.equals(ok, re)) {
                    if (count == tryCount) {
                        Log.i("lbw", "update fail!");
                        return false;
                    }
                    count++;
                } else {
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        bleService.setWriteMode(false);

        //第一层循环 每次取出X轴固件的128个字节数据，并添加上包头包尾(3 字节)，最后一次不足128字节
        while (index < data.length) {
            xPos = 0;
            byte[] temp1 = new byte[131];
            temp1[0] = 0x27;
            if (index + unit1 >= data.length) {
                temp1 = new byte[data.length - index + 3];
                temp1[0] = 0x27;
                temp1[1] = (byte) (data.length - index);
                System.arraycopy(data, index, temp1, 2, data.length - index);
                temp1[temp1.length - 1] = 0x20;
            } else {
                temp1[1] = (byte) 0x80;
                System.arraycopy(data, index, temp1, 2, unit1);
                temp1[130] = 0x20;
            }
            index += unit1;

            count = 0;
            ok:
            while (true) {
                while (xPos < temp1.length) {
                    byte[] temp2 = new byte[20];
                    temp2[0] = (byte) 0xAA;
                    temp2[1] = 0x5A;
                    if (xPos + unit2 >= temp1.length) {
                        temp2[2] = (byte) (temp1.length - xPos);
                        System.arraycopy(temp1, xPos, temp2, 3, temp1.length - xPos);
                        latch = new CountDownLatch(1);
                        msgBody = new MsgBody(latch, temp2);
                        handler.sendMessage(handler.obtainMessage(1, msgBody));
                        try {
                            latch.await();
                            Thread.sleep(delay);
                            num += 128;
                            Log.i("lbw", "===num:" + num);
                            if (!Arrays.equals(ok, re)) {
                                Thread.sleep(2000);
                                xPos = 0;
                                if (count == tryCount) {
                                    Log.i("lbw", "update fail!");
                                    return false;
                                }
                                count++;
                                continue;
                            } else {
                                break ok;
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        bleService.setWriteMode(false);
                        temp2[2] = 0x11;
                        System.arraycopy(temp1, xPos, temp2, 3, unit2);
//                        handler.sendMessage(handler.obtainMessage(0, temp2));
                        latch = new CountDownLatch(1);
                        msgBody = new MsgBody(latch, temp2);
                        handler.sendMessage(handler.obtainMessage(2, msgBody));
                        try {
                            latch.await();
                            Thread.sleep(delay);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    xPos += unit2;
                }
            }
        }
        latch = new CountDownLatch(1);
        msgBody = new MsgBody(latch, cmdBoot);
        handler.sendMessage(handler.obtainMessage(2, msgBody));
        return true;
    }

    private void initData() {
        fileBytes = FileUtil.file2Bytes("sdcard/0425.bin");

        byte[] aa = {fileBytes[0], fileBytes[1], fileBytes[2], fileBytes[3]};
        String xs = Codeutil.bytesToHexString(aa);
        int xLength = Integer.parseInt(xs, 16);
        xBytes = Arrays.copyOfRange(fileBytes, 4, xLength + 4);

        byte[] bb = {fileBytes[xLength + 4], fileBytes[xLength + 5], fileBytes[xLength + 6], fileBytes[xLength + 7]};
        String ys = Codeutil.bytesToHexString(bb);
        int yLength = Integer.parseInt(ys, 16);
        yBytes = Arrays.copyOfRange(fileBytes, 4 + xLength + 4, 4 + xLength + 4 + yLength);

        byte[] cc = {fileBytes[4 + xLength + 4 + yLength], fileBytes[4 + xLength + 4 + yLength + 1], fileBytes[4 + xLength + 4 + yLength + 2], fileBytes[4 + xLength + 4 + yLength + 3]};
        String zs = Codeutil.bytesToHexString(cc);
        int zLength = Integer.parseInt(zs, 16);
        zBytes = Arrays.copyOfRange(fileBytes, 4 + xLength + 4 + yLength + 4, fileBytes.length);


        testBytes = Arrays.copyOfRange(fileBytes, 0, 10240);

        Log.i("lbw", "===lengthX:" + xBytes.length);
        Log.i("lbw", "===lengthY:" + yBytes.length);
        Log.i("lbw", "===lengthZ:" + zBytes.length);
        Log.i("lbw", "===lengthT:" + testBytes.length);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
        bleService.setWriteMode(true);
        handler.removeCallbacksAndMessages(null);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(BleService.TAG_SEND_SERVICE)})
    public void event(BleService service) {

        if (bleService == null) {
            bleService = service;
            bleService.clean();
        }
    }
}
