package com.jiyi.cradleheadcamera.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.jiyi.cradleheadcamera.R;


/**
 * Created by Administrator on 2017/4/6.
 */

public class CameraGirdView extends View {
    int gridType = 0;//0无 1井字 2对角线 3中心点
    Paint paint;
    int width, height;
    Bitmap centerPoint;

    public CameraGirdView(Context context) {
        super(context);
        initView();
    }

    public CameraGirdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public void setGridType(int gridType) {
        this.gridType = gridType;
        invalidate();
    }

    private void initView() {
        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(2);
        DisplayMetrics dm = getResources().getDisplayMetrics();
        width = dm.widthPixels;
        height = dm.heightPixels;
        centerPoint = BitmapFactory.decodeResource(getResources(), R.mipmap.camera_centerpoints);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        switch (gridType) {
            case 0:
                break;
            case 1:
                canvas.drawLine(width / 3, 0, width / 3, height, paint);
                canvas.drawLine(width / 3 * 2, 0, width / 3 * 2, height, paint);
                canvas.drawLine(0, height / 3, width, height / 3, paint);
                canvas.drawLine(0, height / 3 * 2, width, height / 3 * 2, paint);
                break;
            case 2:
                canvas.drawLine(width / 3, 0, width / 3, height, paint);
                canvas.drawLine(width / 3 * 2, 0, width / 3 * 2, height, paint);
                canvas.drawLine(0, height / 3, width, height / 3, paint);
                canvas.drawLine(0, height / 3 * 2, width, height / 3 * 2, paint);
                canvas.drawLine(0, 0, width, height, paint);
                canvas.drawLine(0, height, width, 0, paint);
                break;
            case 3:
                canvas.drawBitmap(centerPoint, width / 2 - centerPoint.getWidth() / 2, height / 2 - centerPoint.getHeight() / 2, null);
                break;
        }
    }
}
