package com.jiyi.cradleheadcamera.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import jp.co.cyberagent.android.gpuimage.GPUImageView;

/**
 * Created by IDesigner on 2017/5/26.
 */

public class MyGpuImageView extends GPUImageView {
    public MyGpuImageView(Context context) {
        super(context);
    }

    public MyGpuImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void savePhoto(String path) {
        Bitmap bm = null;
        try {
            bm = capture();
            File myCaptureFile = new File(path);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));
            bm.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bm.recycle();
            bm = null;
        }
    }

    public void saveImage(String path) {
        File file = new File(path);
        FileOutputStream out = null;
        Bitmap bitmap = getGPUImage().getBitmapWithFilterApplied();
        try {
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (bitmap != null) {
                    bitmap.recycle();
                    bitmap = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
