package com.jiyi.cradleheadcamera.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.jiyi.cradleheadcamera.R;

/**
 * Created by IDesigner on 2017/6/12.
 */

public class FocusSeekView extends LinearLayout {

    ImageView focusView;

    public FocusSeekView(Context context) {
        super(context);
        init(context);
    }

    public FocusSeekView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    private void init(Context context) {
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_focusseek, null);
        focusView = (ImageView) view.findViewById(R.id.focusView);
        addView(view);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Log.i("lbw", "x:" + event.getX() + " t:" + " y:" + event.getY());
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) focusView.getLayoutParams();
            Log.i("lbw", "l:" + layoutParams.leftMargin + " t:" + layoutParams.topMargin+" r:"+layoutParams.rightMargin+" b:"+layoutParams.bottomMargin);
            layoutParams.topMargin = 100;
//            focusView.setLayoutParams(layoutParams);


        }
        return super.onTouchEvent(event);
    }
}
