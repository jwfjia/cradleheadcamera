package com.jiyi.cradleheadcamera.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by IDesigner on 2017/5/9.
 */

public class DrawRectView extends View {

    Paint paint;
    int ax, ay, bx, by;
    int centerX, centerY;
    //ax-left ay-top bx-right by-bottom
    OnTouchScreenListener onTouchScreenListener;

    public DrawRectView(Context context) {
        super(context);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(5);
        paint.setColor(Color.argb(255, 0, 255, 255));
    }

    public DrawRectView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawRectView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void updateRectPostion(int ax, int ay, int bx, int by) {
        this.ax = ax;
        this.ay = ay;
        this.bx = bx;
        this.by = by;
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                onTouchScreenListener.onDown();
                ax = (int) event.getX();
                ay = (int) event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                bx = (int) event.getX();
                by = (int) event.getY();
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                if (onTouchScreenListener != null) {
                    if (ax < bx && ay < by) {
                        onTouchScreenListener.onUp(ax, ay, bx, by);
                    }
                }
                break;
        }

        return super.onTouchEvent(event);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(ax, ay, bx, by, paint);
        Log.i("lbw","===onDraw");
    }

    public void setOnTouchScreenListener(OnTouchScreenListener onTouchScreenListener) {
        this.onTouchScreenListener = onTouchScreenListener;
    }

    public interface OnTouchScreenListener {
        void onUp(int ax, int ay, int bx, int by);
        void onDown();
    }
}
