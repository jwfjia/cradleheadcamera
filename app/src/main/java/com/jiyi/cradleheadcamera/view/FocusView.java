package com.jiyi.cradleheadcamera.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.jiyi.cradleheadcamera.R;


/**
 * Created by Administrator on 2017/4/1.
 */

public class FocusView extends View {

    Bitmap djk;
    float djkX = -1000, djkY = 200;
    int bitmapWidth, bitmapHeight;
    int downCount = 0;
    OnFocusViewChangedListener onFocusViewChangedListener;
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            onFocusViewChangedListener.onGone();
        }
    };

    public FocusView(Context context) {
        super(context);
        initBitmap();
    }


    public FocusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initBitmap();
    }

    public void setOnFocusViewChangedListener(OnFocusViewChangedListener onFocusViewChangedListener) {
        this.onFocusViewChangedListener = onFocusViewChangedListener;
    }

    private void initBitmap() {
        djk = BitmapFactory.decodeResource(getResources(), R.mipmap.djk);
        bitmapWidth = djk.getWidth();
        bitmapHeight = djk.getHeight();
    }

    public void setEvent(MotionEvent event) {
        djkX = event.getX() - bitmapWidth / 2;
        djkY = event.getY() - bitmapHeight / 2;
        invalidate();
        downCount++;
        new MyThread(downCount).start();
        Log.i("lbw", "===set event");
    }

    public void updateDownCount() {
        downCount++;
    }

    public void startNewThread(){
        new MyThread(downCount).start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                onFocusViewChangedListener.onVisible();
                djkX = event.getX() - bitmapWidth / 2;
                djkY = event.getY() - bitmapHeight / 2;
                invalidate();
                downCount++;
                new MyThread(downCount).start();
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(djk, djkX, djkY, null);
    }

    private class MyThread extends Thread {
        int count;

        public MyThread(int count) {
            this.count = count;
        }

        @Override
        public void run() {
            super.run();
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (this.count == downCount) {
                handler.sendEmptyMessage(1);
                djkX = -1000;
                djkY = 200;
                postInvalidate();
            }
        }
    }

    public interface OnFocusViewChangedListener {
        void onVisible();

        void onGone();
    }

}
