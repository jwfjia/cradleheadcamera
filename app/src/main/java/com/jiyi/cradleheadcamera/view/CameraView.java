package com.jiyi.cradleheadcamera.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.base.JiyiApplication;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.util.CameraUtil;
import com.jiyi.cradleheadcamera.util.ImageUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * Created by Administrator on 2017/3/29.
 */

public class CameraView extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {

    public boolean isRecording = false;
    public boolean isTimeLapseMode = false;
    double timeLapseValue = 2;
    int maxDuration = 4 * 1000;
    int quality = 1;
    String currentVideoPath;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    String path = (String) msg.obj;
                    if (onCameraStateChangedListener != null) {
                        onCameraStateChangedListener.onPhotoSaved(path);
                    }
                    break;
            }
        }
    };


    OnCameraStateChangedListener onCameraStateChangedListener;
    MediaRecorder mRecorder;
    SurfaceHolder surfaceHolder;
    Camera camera;
    Context context;
    public int screenWidth, screenHeight, previewWidth, previewHeight;
    public int cameraPosition = 0;
    boolean takePhotosFlag = false;
    String photoPath;
    int videoWidth = 0, videoHeight = 0;
    String whiteBalance = Camera.Parameters.WHITE_BALANCE_AUTO;

    public CameraView(Context context) {
        this(context, null);
    }

    public CameraView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initScreen(context);
        initHolder();
        initCamera();
        initListener();
    }


    private void initListener() {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    camera.autoFocus(new Camera.AutoFocusCallback() {
                        @Override
                        public void onAutoFocus(boolean success, Camera camera) {
                            if (success) {
                                camera.cancelAutoFocus();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void initCamera() {
        if (camera == null) {
            camera = Camera.open(cameraPosition);
        }
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
//            String isoSpeedValues =parameters.get("iso-speed-values");
//            String isoValues =parameters.get("iso-values");
//            parameters.set("iso-speed-values",1600);
//            parameters.set("iso-values", 1600);

//            parameters.setSceneMode(Camera.Parameters.SCENE_MODE_HDR);
            if (JiyiApplication.backCameraPictureSizeList == null) {
                JiyiApplication.backCameraPictureSizeList = parameters.getSupportedPictureSizes();
            }
            if (JiyiApplication.backCameraVideoSizeList == null) {
                JiyiApplication.backCameraVideoSizeList = parameters.getSupportedVideoSizes();
            }
            if (JiyiApplication.isoValues == null) {
                String isoValues = parameters.get("iso-speed-values");
                if (isoValues == null) {
                    isoValues = parameters.get("iso-values");
                }
                JiyiApplication.isoValues = isoValues;
            }
            Camera.Size size = CameraUtil.findBestPreviewResolution(parameters, screenHeight, screenWidth);
            parameters.setPreviewSize(768, 432);
            Camera.Size size2 = CameraUtil.findBestPictureResolution(parameters, screenHeight, screenWidth, quality);
            parameters.setPictureSize(size2.width, size2.height);
            previewWidth = parameters.getPreviewSize().width;
            previewHeight = parameters.getPreviewSize().height;
            parameters.setPictureFormat(PixelFormat.JPEG);
            parameters.setJpegQuality(100);
            camera.setPreviewCallbackWithBuffer(this);
            camera.addCallbackBuffer(new byte[((parameters.getPreviewSize().width * parameters.getPreviewSize().height) * ImageFormat.getBitsPerPixel(ImageFormat.NV21)) / 8]);
            camera.setParameters(parameters);
            try {
                camera.setPreviewDisplay(surfaceHolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            camera.cancelAutoFocus();
            camera.startPreview();
        }
    }

    public void setIso(String value) {
        Camera.Parameters params = camera.getParameters();
        //红米
        params.set("iso-speed-values", value);
        //三星
        params.set("iso-values", value);
        camera.setParameters(params);
    }


    public int getPictureListSize() {
        return camera.getParameters().getSupportedPictureSizes().size();
    }

    public int getVideoListSize() {
        return camera.getParameters().getSupportedVideoSizes().size();
    }

    public void setPictureSize(int pos) {
        Camera.Parameters params = camera.getParameters();
        List<Camera.Size> list = params.getSupportedPictureSizes();
        if (pos == -1) {
            params.setPictureSize(list.get(list.size() / 2).width, list.get(list.size() / 2).height);
        } else {
            params.setPictureSize(list.get(pos).width, list.get(pos).height);
        }
        camera.setParameters(params);
    }

    public void setVideoSize(int pos) {
        Camera.Parameters params = camera.getParameters();
        List<Camera.Size> list = params.getSupportedVideoSizes();
        if (pos == -1) {
            videoWidth = list.get(list.size() / 2).width;
            videoHeight = list.get(list.size() / 2).height;
        } else {
            videoWidth = list.get(pos).width;
            videoHeight = list.get(pos).height;
        }
    }


    public void setVideoSize(int w, int h) {
        videoHeight = h;
        videoWidth = w;
    }

    public void setExposureCompensation(int value) {
        Camera.Parameters params = camera.getParameters();
        params.setExposureCompensation(value);
        camera.setParameters(params);
    }

    public void setFlashLight(String mode) {
        Camera.Parameters parameters = camera.getParameters();
        if (parameters.getSupportedFlashModes() == null) {
            return;
        }
        String flashMode = parameters.getFlashMode();
        List<String> supportedModes = parameters.getSupportedFlashModes();
        if (Camera.Parameters.FLASH_MODE_OFF.equals(flashMode)
                && supportedModes.contains(Camera.Parameters.FLASH_MODE_ON)) {//关闭状态
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            camera.setParameters(parameters);
//            flashBtn.setImageResource(R.drawable.camera_flash_on);
        } else if (Camera.Parameters.FLASH_MODE_ON.equals(flashMode)) {//开启状态
            if (supportedModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
//                camera.setImageResource(R.drawable.camera_flash_auto);
                camera.setParameters(parameters);
            } else if (supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
//                camera.setImageResource(R.drawable.camera_flash_off);
                camera.setParameters(parameters);
            }
        } else if (Camera.Parameters.FLASH_MODE_AUTO.equals(flashMode)
                && supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(parameters);
//            flashBtn.setImageResource(R.drawable.camera_flash_off);
        }
    }

    public void setFlashLightMode(String mode) {
        Camera.Parameters parameters = camera.getParameters();
        if (parameters.getSupportedFlashModes() == null) {
            return;
        }
        parameters.setFlashMode(mode);
        camera.setParameters(parameters);
    }

    public void setZoom(boolean near) {
        Camera.Parameters params = camera.getParameters();
        if (!params.isZoomSupported()) {
            return;
        }
        int unit = params.getMaxZoom() / 10;
        int currentZoom = params.getZoom();
        if (near) {
            currentZoom += unit;
            if (currentZoom > params.getMaxZoom()) {
                currentZoom = params.getMaxZoom();
            }
        } else {
            currentZoom -= unit;
            if (currentZoom < 0) {
                currentZoom = 0;
            }
        }
        if (!params.isSmoothZoomSupported()) {
            params.setZoom(currentZoom);
            camera.setParameters(params);
            return;
        } else {
            camera.startSmoothZoom(currentZoom);
        }
    }


    public void setZoom(int delta) {
        int curZoomValue = 0;
        try {
            Camera.Parameters params = camera.getParameters();
            curZoomValue = (int) (((double) delta / (double) 100) * (double) params.getMaxZoom());
            if (!params.isZoomSupported()) {
                return;
            }
            if (curZoomValue < 0) {
                curZoomValue = 0;
            } else if (curZoomValue > params.getMaxZoom()) {
                curZoomValue = params.getMaxZoom();
            }

            if (!params.isSmoothZoomSupported()) {
                params.setZoom(curZoomValue);
                camera.setParameters(params);
                return;
            } else {
                camera.startSmoothZoom(curZoomValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setWhiteBalanceValue(String wbType) {
        Camera.Parameters parameters = camera.getParameters();
        parameters.setWhiteBalance(wbType);
        camera.setParameters(parameters);
        whiteBalance = wbType;
    }


    public void setPictureSize(int width, int height) {
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            parameters.setPictureSize(width, height);
            camera.setParameters(parameters);
        }
    }


    public void takePhotos(final int times, final int period, final String path) {
        photoPath = path;
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    int i = 0;
                    while (i < times) {
                        i++;
                        takePhotosFlag = true;
                        Thread.sleep(period);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void takeOneJpgPhoto(final String path, final boolean isPortrait) {
        camera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(final byte[] data, Camera camera) {
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        String time = "" + System.currentTimeMillis();
                        handler.sendMessage(handler.obtainMessage(0, path + "pic" + time + ".jpg"));
                        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                        if (cameraPosition == 1) {
                            if (isPortrait) {
                                bmp = ImageUtils.rotateBitmap(bmp, -90);
                            }
                        } else {
                            if (isPortrait) {
                                bmp = ImageUtils.rotateBitmap(bmp, 90);
                            }
                        }
                        ImageUtils.saveBitmapFile(bmp, new File(path + "pic" + time + ".jpg"));
                    }
                }.start();
                camera.startPreview();
            }
        });
    }


    private void initHolder() {
        surfaceHolder = getHolder();
        surfaceHolder.setKeepScreenOn(true);
        surfaceHolder.setFixedSize(screenWidth, screenHeight);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        surfaceHolder.addCallback(this);
    }

    private void initScreen(Context context) {
        WindowManager WM = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        WM.getDefaultDisplay().getMetrics(outMetrics);
        screenWidth = outMetrics.widthPixels;
        screenHeight = outMetrics.heightPixels;
    }

    public Camera getCamera() {
        return camera;
    }

    public void changeCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = 0;
        cameraCount = Camera.getNumberOfCameras();
        if (cameraCount > 1) {
            if (cameraPosition == 0) {
                cameraPosition = 1;
            } else {
                cameraPosition = 0;
            }
            releaseCamera();
            camera = Camera.open(cameraPosition);
            Camera.Parameters parameters = camera.getParameters();
            if (cameraPosition == 0) {
                JiyiApplication.backCameraPictureSizeList = parameters.getSupportedPictureSizes();
                JiyiApplication.backCameraVideoSizeList = parameters.getSupportedVideoSizes();
            } else {
                JiyiApplication.frontCameraPictureSizeList = parameters.getSupportedPictureSizes();
                JiyiApplication.frontCameraVideoSizeList = parameters.getSupportedVideoSizes();
            }
            Camera.Size size = CameraUtil.findBestPreviewResolution(parameters, screenHeight, screenWidth);
            parameters.setPreviewSize(768, 432);
            Camera.Size size2 = CameraUtil.findBestPictureResolution(parameters, screenHeight, screenWidth, quality);
            parameters.setPictureSize(size2.width, size2.height);
            parameters.setWhiteBalance(whiteBalance);
            previewWidth = parameters.getPreviewSize().width;
            previewHeight = parameters.getPreviewSize().height;
            parameters.setPictureFormat(PixelFormat.JPEG);
//            parameters.setPictureSize(pictureWidth, pictureHeight);
            parameters.setJpegQuality(100);
            camera.setPreviewCallbackWithBuffer(this);
            camera.addCallbackBuffer(new byte[((parameters.getPreviewSize().width * parameters.getPreviewSize().height) * ImageFormat.getBitsPerPixel(ImageFormat.NV21)) / 8]);
            camera.setParameters(parameters);
            try {
                camera.setPreviewDisplay(surfaceHolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            camera.startPreview();
        } else {
            Toast.makeText(context, "该设备只有一个摄像头", Toast.LENGTH_SHORT).show();
        }
        onCameraStateChangedListener.onCameraChanged();
    }


    public void releaseCamera() {
        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }


    public void startRecord(final String path, final boolean isPortrait) {
        RxBus.get().post(Constants.TAG_START_TIME_COUNT, true);
        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
            mRecorder.reset();
        }

        if (camera != null) {
            camera.unlock();
            mRecorder.setCamera(camera);

        }
        if (cameraPosition == 1) {
            if (isPortrait) {
                mRecorder.setOrientationHint(270);
            } else {
                mRecorder.setOrientationHint(0);
            }
        } else {
            if (isPortrait) {
                mRecorder.setOrientationHint(90);
            } else {
                mRecorder.setOrientationHint(0);
            }
        }

        try {
            if (isTimeLapseMode) {
                mRecorder.setCaptureRate(timeLapseValue);
                mRecorder.setMaxDuration(maxDuration);
                mRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                    @Override
                    public void onInfo(MediaRecorder mr, int what, int extra) {
                        if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                            stopRecord();
                        }
                    }
                });
            }
            //设置音频采集方式
            mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            //设置视频的采集方式
            mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            //设置文件的输出格式
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);//aac_adif， aac_adts， output_format_rtp_avp， output_format_mpeg2ts ，webm
            //设置audio的编码格式
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            //设置video的编码格式
            mRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            //设置录制的视频编码比特率
            mRecorder.setVideoEncodingBitRate(1024 * 1024 * 5);
            //设置录制的视频帧率,注意文档的说明:
            mRecorder.setVideoFrameRate(30);

            if (videoWidth == 0) {
                //设置要捕获的视频的宽度和高度
                surfaceHolder.setFixedSize(640, 480);
                mRecorder.setVideoSize(640, 480);
            } else {
                surfaceHolder.setFixedSize(videoWidth, videoHeight);
                mRecorder.setVideoSize(videoWidth, videoHeight);
            }
            mRecorder.setPreviewDisplay(surfaceHolder.getSurface());
            //设置输出文件的路径
            String time = "" + System.currentTimeMillis();
            currentVideoPath = path + "video" + time + ".mp4";
            mRecorder.setOutputFile(currentVideoPath);
            //准备录制
            mRecorder.prepare();
            //开始录制
            mRecorder.start();
            isRecording = true;
        } catch (Exception e) {
            stopRecord();
            e.printStackTrace();
        }

    }

    public void setTimeLapseMode(boolean b) {
        isTimeLapseMode = b;
    }

    public void setTimeLapseValue(double d) {
        timeLapseValue = d;
    }

    public void setMaxTime(int max) {
        maxDuration = max;
    }

    public void stopRecord() {
        RxBus.get().post(Constants.TAG_START_TIME_COUNT, false);
        if (mRecorder != null) {
            try {
                //停止录制
                mRecorder.stop();
                //重置
                mRecorder.reset();
                isRecording = false;
                releaseMediaRecorder();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (onCameraStateChangedListener != null) {
                            onCameraStateChangedListener.onVideoSaved(currentVideoPath);
                        }
                    }
                }, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void releaseMediaRecorder() {
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
            camera.lock();
        }
    }


    //Override methods

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (camera != null)
                camera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        releaseCamera();
        releaseMediaRecorder();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        camera.addCallbackBuffer(data);
        if (onCameraStateChangedListener != null) {
            onCameraStateChangedListener.onCurrentPreviewFrame(data);
        }
        //处理帧
        if (takePhotosFlag) {
            takePhotosFlag = false;
            Camera.Parameters parameters = camera.getParameters();
//            new SavePhotoThread(data, parameters.getPreviewSize().width, parameters.getPreviewSize().height, parameters.getPictureListSize().width, parameters.getPictureListSize().height, photoPath).start();
        }
    }


    public interface OnCameraStateChangedListener {
        void onPhotoSaved(String filePath);

        void onVideoSaved(String filePath);

        void onCameraChanged();

        void onCurrentPreviewFrame(byte[] frame);
    }

    public void setOnCameraStateChangedListener(OnCameraStateChangedListener onCameraStateChangedListener) {
        this.onCameraStateChangedListener = onCameraStateChangedListener;
    }


}
