package com.jiyi.cradleheadcamera.util;

import android.util.Log;

import java.io.ByteArrayOutputStream;

public class Codeutil {
    private static String hexString = "0123456789ABCDEF";

    /*
     * 将字符串换为16进制
     */
    public static String StringTohexString(String str) {
        // 根据默认编码获取字节数组
        byte[] bytes = str.getBytes();
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        // 将字节数组中每个字节拆解成2位16进制整数
        for (int i = 0; i < bytes.length; i++) {
            sb.append(hexString.charAt((bytes[i] & 0xf0) >> 4));
            sb.append(hexString.charAt((bytes[i] & 0x0f) >> 0));
        }
        return sb.toString();
    }

    /*
     * 将16进制数字解码成字符串,适用于所有字符（包括中文）
     */
    public static String hexStringToString(String bytes) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(
                bytes.length() / 2);
        // 将每2位16进制整数组装成一个字节
        for (int i = 0; i < bytes.length(); i += 2)
            baos.write((hexString.indexOf(bytes.charAt(i)) << 4 | hexString
                    .indexOf(bytes.charAt(i + 1))));
        return new String(baos.toByteArray());
    }

    /*
     * 把16进制字符串转换成字节数组
     */
    public static byte[] hexStringToByte(String hex) {
        int len = (hex.length() / 2);
        byte[] result = new byte[len];
        char[] achar = hex.toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
        }
        return result;
    }

    private static int toByte(char c) {
        byte b = (byte) hexString.indexOf(c);
        return b;
    }

    /*
     * 数组转换成十六进制字符串
     */
    public static String bytesToHexString(byte[] bArray) {
        if (bArray == null) {
            return "null";
        }
        StringBuffer sb = new StringBuffer(bArray.length);
        String sTemp;
        for (int i = 0; i < bArray.length; i++) {
            sTemp = Integer.toHexString(0xFF & bArray[i]);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase());
        }
        return sb.toString();
    }

    public final static String qpDecoding(String str) {
        if (str == null) {
            return "";
        }
        try {
            StringBuffer sb = new StringBuffer(str);
            for (int i = 0; i < sb.length(); i++) {
                if (sb.charAt(i) == '\n' && sb.charAt(i - 1) == '=') {
                    // 解码这个地方也要修改一下
                    // sb.deleteCharAt(i);
                    sb.deleteCharAt(i - 1);
                }
            }
            str = sb.toString();
            byte[] bytes = str.getBytes("US-ASCII");
            for (int i = 0; i < bytes.length; i++) {
                byte b = bytes[i];
                if (b != 95) {
                    bytes[i] = b;
                } else {
                    bytes[i] = 32;
                }
            }
            if (bytes == null) {
                return "";
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            for (int i = 0; i < bytes.length; i++) {
                int b = bytes[i];
                if (b == '=') {
                    try {
                        int u = Character.digit((char) bytes[++i], 16);
                        int l = Character.digit((char) bytes[++i], 16);
                        if (u == -1 || l == -1) {
                            continue;
                        }
                        buffer.write((char) ((u << 4) + l));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                } else {
                    buffer.write(b);
                }
            }
            return new String(buffer.toByteArray(), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static byte[] convertCoordinate(int[] bb_ret, int previewWidth, int previewHeight,int front_cam) {//1920 1080
        int left_offset = 1920 / 2;
        int top_offset = 1080 / 2;
        int limit = 600;


        if(bb_ret[4] ==0 || bb_ret[0] > 1920 || bb_ret[0]<0 || bb_ret[1] > 1080 || bb_ret[1]<0 )
        {
            bb_ret[0] = left_offset-1;
            bb_ret[1] = top_offset+1;
        }

        int bb_left = (bb_ret[0] - left_offset) <= 0 ? (int)(-(bb_ret[0] - left_offset) *1.5) : 0;
        int bb_right = (bb_ret[0] - left_offset) >= 0 ? (int)((bb_ret[0] - left_offset) *1.5) : 0;
        int bb_top = (bb_ret[1] - top_offset) <= 0 ? -(bb_ret[1] - top_offset) * 2 : 0;
        int bb_bottom = (bb_ret[1] - top_offset) >= 0 ? (bb_ret[1] - top_offset) * 2 : 0;



        if(bb_left > limit) bb_left = limit;
        else if(bb_left < -limit) bb_left = -limit;

        if(bb_right > limit) bb_right = limit;
        else if(bb_right < -limit) bb_right = -limit;

        if(bb_top > limit) bb_top = limit;
        else if(bb_top < -limit) bb_top = -limit;

        if(bb_bottom > limit) bb_bottom = limit;
        else if(bb_bottom < -limit) bb_bottom = -limit;


        int temp1 = bb_ret[0]-left_offset;
        int temp2 = -(bb_ret[1]-top_offset);



//        Log.i("", "convert:" + bb_left + " " + bb_right + " " + bb_top + " " + bb_bottom + " " +temp1 + " " + temp2 + " " +left_offset + " " + top_offset);
        byte[] data = new byte[12];
        data[0] = (byte) 0xAA;
        data[1] = (byte) 0x57;
        data[2] = (byte) 0x09;
        byte sum_check = 0;
        data[3] = (byte) (bb_right >> 8);
        data[4] = (byte) (bb_right & 0xFF);

        data[7] = (byte) (bb_left >> 8);
        data[8] = (byte) (bb_left & 0xFF);

        if(front_cam==1)
        {
            data[3] = (byte) (bb_right >> 8);
            data[4] = (byte) (bb_right & 0xFF);

            data[5] = (byte) (bb_bottom >> 8);
            data[6] = (byte) (bb_bottom & 0xFF);

            data[7] = (byte) (bb_left >> 8);
            data[8] = (byte) (bb_left & 0xFF);



            data[9] = (byte) (bb_top >> 8);
            data[10] = (byte) (bb_top & 0xFF);



        }
        else
        {
            data[3] = (byte) (bb_left >> 8);
            data[4] = (byte) (bb_left & 0xFF);

            data[5] = (byte) (bb_top >> 8);
            data[6] = (byte) (bb_top & 0xFF);

            data[7] = (byte) (bb_right >> 8);
            data[8] = (byte) (bb_right & 0xFF);

            data[9] = (byte) (bb_bottom >> 8);
            data[10] = (byte) (bb_bottom & 0xFF);
        }

        for (int j = 0; j < 8; j++) {
            sum_check += data[3 + j];
        }
        data[11] = (byte) (sum_check & 0xFF);
        return data;
    }
}
