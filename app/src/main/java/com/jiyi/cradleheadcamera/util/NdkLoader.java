package com.jiyi.cradleheadcamera.util;

/**
 * Created by IDesigner on 2017/5/22.
 */

public class NdkLoader {
    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("hello-neon");
        System.loadLibrary("pano");
    }

    public static native int[] salt(byte[] yuvdata, int width, int height,int cameraid);

    public static native void bbox(int bbx, int bby, int ccx, int ccy);

    public static native long nativeCreateObject();

    public static native void release();

    public static native void endruning();

    public static native int stitch(String[] source,String result,double scale);

    public static native void Threshold(int threshold);
}
