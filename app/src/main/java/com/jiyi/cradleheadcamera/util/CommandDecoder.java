package com.jiyi.cradleheadcamera.util;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.bean.DeviceStatus;
import com.jiyi.cradleheadcamera.common.Constants;

/**
 * Created by IDesigner on 2017/5/16.
 */

public class CommandDecoder {

    public static void parseType(byte[] data) {
        if (data[0] == 0x55) {
            switch (data[1]) {
                case (byte) 0xAA:
                    parseStatus(data);
                    break;
                case (byte) 0xAB:
                    parseAction(data);
                    break;
            }
        }

    }

    public static void parseAction(byte[] data) {
        String cmd = Codeutil.bytesToHexString(data);
        switch (cmd) {
            case "55AB050100000001":
                RxBus.get().post(Constants.TAG_DIALOG_CAMERA_SHUTTER, "");
                break;
            case "55AB050200000002":
                RxBus.get().post(Constants.TAG_DIALOG_CAMERA_RECORD, "");
                break;
            case "55AB050001000001":
                RxBus.get().post(Constants.TAG_DIALOG_CAMERA_ZOOM, true);
                break;
            case "55AB050002000002":
                RxBus.get().post(Constants.TAG_DIALOG_CAMERA_ZOOM, false);
                break;
        }
    }


    public static DeviceStatus parseStatus(byte[] data) {
        String workStatus, workMode, proximity;
        switch (data[5]) {
            case 0x00:
                workStatus = "休眠";
                break;
            case 0x01:
                workStatus = "正常";
                break;
            case 0x02:
                workStatus = "目标丢失";
                break;
            case 0x03:
                workStatus = "X轴到达极限";
                break;
            case 0x04:
                workStatus = "Y轴到达极限";
                break;
            case 0x05:
                workStatus = "Z轴到达极限";
                break;
            default:
                workStatus = "未知";
                break;
        }

        switch (data[6]) {
            case 0x01:
                workMode = "全景";
                RxBus.get().post(Constants.TAG_PANO_READY, 1);
                break;
            case 0x02:
                workMode = "全景";
                RxBus.get().post(Constants.TAG_PANO_END, 1);
                break;
            case 0x03:
                workMode = "跟踪";
                break;
            case 0x04:
                workMode = "延时摄影";
                break;
            case 0x05:
                workMode = "休眠";
                break;
            case 0x06:
                workMode = "航向跟随";
                break;
            case 0x07:
                workMode = "锁定";
                break;
            case 0x08:
                workMode = "全跟随";
                break;
            default:
                workMode = "未知";
                break;
        }
        switch (data[9]) {
            case 0x00:
                proximity = "未感应到手机";
                break;
            case 0x01:
                proximity = "感应到手机";
                break;
            default:
                proximity = "未知";
                break;
        }
        DeviceStatus deviceStatus = new DeviceStatus(data[3] + "" + data[4], workStatus, workMode, proximity);
        RxBus.get().post(Constants.TAG_DEVICE_STATUS, deviceStatus);
        return deviceStatus;
    }
}
