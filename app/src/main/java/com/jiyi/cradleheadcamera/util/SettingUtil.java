package com.jiyi.cradleheadcamera.util;

import com.jiyi.cradleheadcamera.base.JiyiApplication;

/**
 * Created by IDesigner on 2017/5/19.
 */

public class SettingUtil {
    public static void setFrontCameraPictureSizePostion(int pos) {
        PreferencesUtils.putInt(JiyiApplication.getContextObject(), "fcp", pos);
    }

    public static void setFrontCameraVideoSizePostion(int pos) {
        PreferencesUtils.putInt(JiyiApplication.getContextObject(), "fcv", pos);
    }

    public static void setBackCameraPictureSizePostion(int pos) {
        PreferencesUtils.putInt(JiyiApplication.getContextObject(), "bcp", pos);
    }

    public static void setBackCameraVideoSizePostion(int pos) {
        PreferencesUtils.putInt(JiyiApplication.getContextObject(), "bcv", pos);
    }


    public static int getFrontCameraPictureSizePostion() {
        return PreferencesUtils.getInt(JiyiApplication.getContextObject(), "fcp", -1);
    }

    public static int getFrontCameraVideoSizePostion() {
        return PreferencesUtils.getInt(JiyiApplication.getContextObject(), "fcv", -1);
    }

    public static int getBackCameraPictureSizePostion() {
        return PreferencesUtils.getInt(JiyiApplication.getContextObject(), "bcp", -1);
    }

    public static int getBackCameraVideoSizePostion() {
        return PreferencesUtils.getInt(JiyiApplication.getContextObject(), "bcv", -1);
    }

    public static void setExposureCompensation(int value) {
        PreferencesUtils.putInt(JiyiApplication.getContextObject(), "ExposureCompensation", value);
    }

    public static int getExposureCompensation() {
        return PreferencesUtils.getInt(JiyiApplication.getContextObject(), "ExposureCompensation", 0);
    }

    public static void setIsoValue(String value) {
        PreferencesUtils.putString(JiyiApplication.getContextObject(), "iso", value);
    }

    public static String getIsoValue() {
        return PreferencesUtils.getString(JiyiApplication.getContextObject(), "iso", "auto");
    }
}
