package com.jiyi.cradleheadcamera;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.util.FileUtil;
import com.jiyi.cradleheadcamera.util.NdkLoader;
import com.jiyi.cradleheadcamera.view.TouchImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import top.zibin.luban.Luban;

public class PhotoPreviewActivity extends AppCompatActivity {


    ProgressDialog progressDialog;
    @Bind(R.id.photo_preview_edit)
    TextView photoPreviewEdit;
    @Bind(R.id.photo_preview_img)
    PhotoView photoPreviewImg;
    String path;
    String outPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_preview);
        ButterKnife.bind(this);
        outPath = Constants.JIYI_PHOTO_DIRECTORY + "pano" + System.currentTimeMillis() + ".jpg";
        path = getIntent().getStringExtra("path");
        if (path != null) {
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            File file = new File(path);
            Picasso.with(getApplicationContext()).load(file).into(photoPreviewImg);

            photoPreviewEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), PhotoEditActivity.class);
                    intent.putExtra("path", path);
                    startActivity(intent);
                }
            });
        }

        final List<String> paths = (List<String>) getIntent().getSerializableExtra("panoPathList");
        if (paths != null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("拼接中...");
            progressDialog.show();

            Observable.create(new ObservableOnSubscribe<File>() {
                @Override
                public void subscribe(ObservableEmitter<File> e) throws Exception {
                    int size = paths.size();
                    String[] arr = (String[]) paths.toArray(new String[size]);
                    int i = NdkLoader.stitch(arr, outPath, 0.7);
                    FileUtil.deleteFiles(paths);
                    if (i == 0) {
//                        File file = Luban.with(getApplicationContext()).load(new File(outPath)).get();
                        File file = new File(outPath);
                        e.onNext(file);
                        e.onComplete();
                    } else {
                        e.onNext(null);
                        e.onComplete();
                    }
                }
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<File>() {
                        @Override
                        public void accept(File file) throws Exception {
                            if (file == null) {
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "拼接失败，请重新拍摄！", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Picasso.with(getApplicationContext()).load(file).into(photoPreviewImg, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        progressDialog.dismiss();
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                            }
                        }
                    });


        }


    }
}
