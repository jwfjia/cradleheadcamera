package com.jiyi.cradleheadcamera;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.adapter.BtDeviceAdapter;
import com.jiyi.cradleheadcamera.base.BaseActivity;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.service.BleService;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity {

    BleService bleService;
    @Bind(R.id.listview)
    ListView listview;
    List<BluetoothDevice> list = new ArrayList<>();
    BtDeviceAdapter adapter;
    ProgressDialog dialog;
    @Bind(R.id.text)
    TextView text;
    BluetoothGatt tempGatt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        dialog = new ProgressDialog(this);
        dialog.setTitle("connecting");
        adapter = new BtDeviceAdapter(getApplicationContext(), list);
        listview.setAdapter(adapter);
        RxBus.get().register(this);
        RxBus.get().post(BleService.TAG_REQUEST_SERVICE, 1);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (bleService != null) {
                    bleService.connectBleDevice(adapter.getItem(position));
                    dialog.show();
                }
            }
        });
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxBus.get().post("abc", "a");
            }
        });
    }


    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(BleService.TAG_SEND_SERVICE)})
    public void event(BleService service) {
        Log.i("lbw","===TAG_SEND_SERVICE");
        if (bleService == null) {
            bleService = service;
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_SCAN)})
    public void event(BluetoothDevice device) {
        if (!list.contains(device)) {
            list.add(device);
            adapter.initData(list);
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_SERVICE_DISCOVERED)})
    public void event(String i) {
        Log.i("lbw","===TAG_SERVICE_DISCOVERED");
        dialog.dismiss();
        tempGatt = bleService.tempGatt;
        for (BluetoothGattService service : tempGatt.getServices()){
            for (BluetoothGattCharacteristic characteristi:service.getCharacteristics()) {
                if (characteristi.getProperties() == 0x10){
                    Log.i("lbw","===startNotify:"+characteristi.getUuid().toString());
                    bleService.startNotify(service.getUuid().toString(),characteristi.getUuid().toString());
                }
                if (characteristi.getProperties() == 36){
                    Log.i("lbw","===startWrite:"+service.getUuid().toString()+"--"+characteristi.getUuid().toString());
                }
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
        stopService(new Intent(getApplicationContext(), BleService.class));
    }
}
