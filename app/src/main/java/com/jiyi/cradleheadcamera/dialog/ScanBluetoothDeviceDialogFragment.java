package com.jiyi.cradleheadcamera.dialog;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.adapter.BtDeviceAdapter;
import com.jiyi.cradleheadcamera.common.Command;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.service.BleService;
import com.jiyi.cradleheadcamera.util.Codeutil;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IDesigner on 2017/4/20.
 */

public class ScanBluetoothDeviceDialogFragment extends DialogFragment {
    BleService bleService;
    @Bind(R.id.dialog_scan_listview)
    ListView dialogScanListview;
    BtDeviceAdapter adapter;
    List<BluetoothDevice> list = new ArrayList<>();
    @Bind(R.id.dialog_scan_close)
    ImageView dialogScanClose;
    ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_scan, container);
        ButterKnife.bind(this, view);
        RxBus.get().register(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("连接中");
        progressDialog.setCanceledOnTouchOutside(false);
        adapter = new BtDeviceAdapter(getContext(), list);
        dialogScanListview.setAdapter(adapter);
        RxBus.get().post(BleService.TAG_REQUEST_SERVICE, "");
        dialogScanClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        dialogScanListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                progressDialog.show();
                bleService.connectBleDevice(adapter.getItem(position));
            }
        });
        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        list.clear();
        bleService = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getDialog().getWindow().setLayout((int) (dm.widthPixels * 0.8), getDialog().getWindow().getAttributes().height);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        RxBus.get().unregister(this);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_SCAN)})
    public void event(BluetoothDevice device) {
        if (!list.contains(device)) {
            list.add(device);
            adapter.initData(list);
        }
    }


    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(BleService.TAG_SEND_SERVICE)})
    public void event(BleService service) {

        if (bleService == null) {
            bleService = service;
            bleService.scanBleDevice();
        }
    }
    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_SERVICE_DISCOVERED)})
    public void event(BluetoothGatt gatt){
//        for (BluetoothGattService service : gatt.getServices()){
//            for (BluetoothGattCharacteristic characteristi:service.getCharacteristics()){
//                if (characteristi.getProperties()==48){
//                    bleService.startNotify(service.getUuid().toString(),characteristi.getUuid().toString());
//                    Log.i("","===notify uuid:"+service.getUuid().toString()+"   "+characteristi.getUuid().toString());
//                }
//                if (characteristi.getProperties() == 36){
//                    Log.i("lbw","===startWrite:"+service.getUuid().toString()+"--"+characteristi.getUuid().toString());
//                }
//            }
//        }
        bleService.startNotify("98d076f0-d6de-11e6-bf26-cec0c932ce01","98d076f2-d6de-11e6-bf26-cec0c932ce01");

        progressDialog.dismiss();
    }

}
