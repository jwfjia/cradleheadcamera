package com.jiyi.cradleheadcamera.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.adapter.IndexViewPagerAdapter;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.fragment.CameraSettingFlashFragment;
import com.jiyi.cradleheadcamera.fragment.CameraSettingGridFragment;
import com.jiyi.cradleheadcamera.fragment.CameraSettingIndexFragment;
import com.jiyi.cradleheadcamera.fragment.CameraSettingWhiteBalanceFragment;
import com.jiyi.cradleheadcamera.view.CustomViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IDesigner on 2017/4/24.
 */

public class CameraSettingDialogFragment extends DialogFragment implements View.OnClickListener {

    List<Fragment> list = new ArrayList<>();
    IndexViewPagerAdapter adapter;
    CameraSettingIndexFragment settingIndexFragment;
    CameraSettingWhiteBalanceFragment whiteBalanceFragment;
    CameraSettingFlashFragment flashFragment;
    CameraSettingGridFragment gridFragment;
    @Bind(R.id.camera_home)
    ImageView cameraHome;
    @Bind(R.id.camera_camera_setting)
    ImageView cameraCameraSetting;
    @Bind(R.id.camera_cradle_setting)
    ImageView cameraCradleSetting;
    @Bind(R.id.camera_setting)
    ImageView cameraSetting;
    @Bind(R.id.fragment_camera_setting_viewpager)
    CustomViewPager fragmentCameraSettingViewpager;
    @Bind(R.id.fragment_camera_setting_goback)
    ImageView fragmentCameraSettingGoback;
    @Bind(R.id.fragment_camera_setting_close)
    ImageView fragmentCameraSettingClose;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);
        RxBus.get().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_camera_setting, container);
        ButterKnife.bind(this, view);
        initFragment();
        initListener();
        return view;
    }

    private void initFragment() {
        settingIndexFragment = new CameraSettingIndexFragment();
        whiteBalanceFragment = new CameraSettingWhiteBalanceFragment();
        flashFragment = new CameraSettingFlashFragment();
        gridFragment = new CameraSettingGridFragment();
        list.add(settingIndexFragment);
        list.add(whiteBalanceFragment);
        list.add(flashFragment);
        list.add(gridFragment);
        adapter = new IndexViewPagerAdapter(getChildFragmentManager(), list);
        fragmentCameraSettingViewpager.setOffscreenPageLimit(10);
        fragmentCameraSettingViewpager.setAdapter(adapter);
    }

    private void initListener() {
        fragmentCameraSettingClose.setOnClickListener(this);
        fragmentCameraSettingGoback.setOnClickListener(this);
        cameraHome.setOnClickListener(this);
        cameraCameraSetting.setOnClickListener(this);
        cameraCradleSetting.setOnClickListener(this);
        cameraSetting.setOnClickListener(this);
        fragmentCameraSettingViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    fragmentCameraSettingGoback.setVisibility(View.INVISIBLE);
                } else {
                    fragmentCameraSettingGoback.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getDialog().setCanceledOnTouchOutside(true);
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.LEFT;
        lp.x = 0;
        lp.y = -dm.heightPixels;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        window.setLayout((int) (dm.widthPixels * 0.5), getDialog().getWindow().getAttributes().height);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        fragmentCameraSettingViewpager.setCurrentItem(0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_camera_setting:
                dismiss();
                break;
            case R.id.camera_cradle_setting:
                RxBus.get().post(Constants.TAG_DIALOG_CRADLE_SETTING, "");
                break;
            case R.id.fragment_camera_setting_goback:
                fragmentCameraSettingViewpager.setCurrentItem(0);
                break;
            case R.id.fragment_camera_setting_close:
                dismiss();
                break;
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_FRAGMENT_CAMERA_SETTING)})
    public void event(Integer i) {
        fragmentCameraSettingViewpager.setCurrentItem(i);
    }

}
