package com.jiyi.cradleheadcamera.dialog;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.github.glomadrian.materialanimatedswitch.MaterialAnimatedSwitch;
import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.common.Command;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.util.NdkLoader;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IDesigner on 2017/4/25.
 */

public class ShutterSettingDialogFragment extends DialogFragment {

    TimeLapseRecordSettingDialogFragment timeLapseRecordSettingDialogFragment;
    @Bind(R.id.fragment_shutter_setting_rbtn_single)
    RadioButton fragmentShutterSettingRbtnSingle;
    @Bind(R.id.fragment_shutter_setting_rbtn_3s)
    RadioButton fragmentShutterSettingRbtn3s;
    @Bind(R.id.fragment_shutter_setting_rbtn_5s)
    RadioButton fragmentShutterSettingRbtn5s;
    @Bind(R.id.fragment_shutter_setting_rbtn_10s)
    RadioButton fragmentShutterSettingRbtn10s;
    @Bind(R.id.fragment_shutter_setting_radiogroup)
    RadioGroup fragmentShutterSettingRadiogroup;
    @Bind(R.id.camera_photo_mode)
    ImageView cameraPhotoMode;
    @Bind(R.id.fragment_shutter_setting_pano_null)
    RadioButton fragmentShutterSettingPanoNull;
    @Bind(R.id.fragment_shutter_setting_pano180)
    RadioButton fragmentShutterSettingPano180;
    @Bind(R.id.fragment_shutter_setting_pano360)
    RadioButton fragmentShutterSettingPano360;
    @Bind(R.id.fragment_shutter_setting_pano_radiogroup)
    RadioGroup fragmentShutterSettingPanoRadiogroup;
    @Bind(R.id.fragment_record_setting_auto)
    RadioButton fragmentRecordSettingAuto;
    @Bind(R.id.fragment_record_setting_time)
    RadioButton fragmentRecordSettingTime;
    @Bind(R.id.fragment_record_setting_radiogroup)
    RadioGroup fragmentRecordSettingRadiogroup;
    @Bind(R.id.camera_mode_switch)
    MaterialAnimatedSwitch cameraModeSwitch;
    @Bind(R.id.camera_shutter)
    ImageView cameraShutter;
    @Bind(R.id.camera_camera_switch)
    ImageView cameraCameraSwitch;
    @Bind(R.id.camera_photo_display)
    ImageView cameraPhotoDisplay;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);
        RxBus.get().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_shutter_setting, container);
        ButterKnife.bind(this, view);
        timeLapseRecordSettingDialogFragment = new TimeLapseRecordSettingDialogFragment();
        initListener();
        return view;
    }

    private void initListener() {

        fragmentRecordSettingRadiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.fragment_record_setting_auto:
                        RxBus.get().post(Constants.TAG_DIALOG_RECORD_TIME_LAPSE_MODE, false);
                        break;
                    case R.id.fragment_record_setting_time:
                        RxBus.get().post(Constants.TAG_DIALOG_RECORD_TIME_LAPSE_MODE, true);
                        timeLapseRecordSettingDialogFragment.show(getChildFragmentManager(),"dd");
                        break;
                }
            }
        });

        fragmentShutterSettingRadiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.fragment_shutter_setting_rbtn_3s:
                        RxBus.get().post(Constants.TAG_FRAGMENT_SHUTTER_SETTING_CHANGE, R.mipmap.longan_handle_mode_interval_3s_off);
                        RxBus.get().post(Constants.TAG_CAMERA_SHUTTER_DELAY, 3000);
                        cameraPhotoMode.setImageResource(R.mipmap.longan_handle_mode_interval_3s_on);
                        break;
                    case R.id.fragment_shutter_setting_rbtn_5s:
                        RxBus.get().post(Constants.TAG_FRAGMENT_SHUTTER_SETTING_CHANGE, R.mipmap.longan_handle_mode_interval_5s_off);
                        RxBus.get().post(Constants.TAG_CAMERA_SHUTTER_DELAY, 5000);
                        cameraPhotoMode.setImageResource(R.mipmap.longan_handle_mode_interval_5s_on);
                        break;
                    case R.id.fragment_shutter_setting_rbtn_10s:
                        RxBus.get().post(Constants.TAG_FRAGMENT_SHUTTER_SETTING_CHANGE, R.mipmap.longan_handle_mode_interval_10s_off);
                        RxBus.get().post(Constants.TAG_CAMERA_SHUTTER_DELAY, 10000);
                        cameraPhotoMode.setImageResource(R.mipmap.longan_handle_mode_interval_10s_on);
                        break;
                    case R.id.fragment_shutter_setting_rbtn_single:
                        RxBus.get().post(Constants.TAG_FRAGMENT_SHUTTER_SETTING_CHANGE, R.drawable.btn_camera_photo_mode_background);
                        RxBus.get().post(Constants.TAG_CAMERA_SHUTTER_DELAY, 0);
                        cameraPhotoMode.setImageResource(R.mipmap.ic_leftbar_photo_blue);
                        break;
                }
            }
        });

        fragmentShutterSettingPanoRadiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.fragment_shutter_setting_pano_null:
                        RxBus.get().post(Constants.TAG_PANO9,false);
                        RxBus.get().post(Constants.TAG_PANO_MODE, false);
                        RxBus.get().post(Constants.TAG_WRITE, Command.request_pano_end);
                        break;
                    case R.id.fragment_shutter_setting_pano180:
                        RxBus.get().post(Constants.TAG_PANO9,false);
                        RxBus.get().post(Constants.TAG_PANO_MODE, true);
                        RxBus.get().post(Constants.TAG_WRITE, Command.request_pano180_high);
                        break;
                    case R.id.fragment_shutter_setting_pano360:
                        RxBus.get().post(Constants.TAG_PANO9,false);
                        RxBus.get().post(Constants.TAG_PANO_MODE, true);
                        RxBus.get().post(Constants.TAG_WRITE, Command.request_pano320_high);
                        break;
                    case R.id.fragment_shutter_setting_pano9:
                        RxBus.get().post(Constants.TAG_PANO9,true);
                        break;
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getDialog().setCanceledOnTouchOutside(true);

        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.RIGHT;
        lp.x = 0;
        lp.y = -dm.heightPixels;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        window.setLayout((int) (dm.widthPixels * 0.5), getDialog().getWindow().getAttributes().height);
    }

    @Override
    public void onDestroyView() {
        fragmentShutterSettingPanoRadiogroup.setOnCheckedChangeListener(null);
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
