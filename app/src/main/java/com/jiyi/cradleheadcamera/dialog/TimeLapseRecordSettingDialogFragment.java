package com.jiyi.cradleheadcamera.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.util.TimeUtil;
import com.lbw.selectview.SelectView;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IDesigner on 2017/4/20.
 */

public class TimeLapseRecordSettingDialogFragment extends DialogFragment {

    double a = 0.5, b = 1, c = 4, d = 2;
    @Bind(R.id.fragment_record_setting_para)
    SelectView fragmentRecordSettingPara;
    @Bind(R.id.fragment_record_setting_min)
    SelectView fragmentRecordSettingMin;
    @Bind(R.id.fragment_record_setting_length)
    TextView fragmentRecordSettingLength;
    @Bind(R.id.fragment_record_setting_ok)
    Button fragmentRecordSettingOk;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_time_lapse_setting, container);
        ButterKnife.bind(this, view);
        RxBus.get().register(this);
        initData();
        initListener();
        return view;
    }

    private void initData() {
        fragmentRecordSettingLength.setText(TimeUtil.secToTime((int) c));
        String[] data = {"0.5", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "20", "30", "45", "60"};
        List<String> list1 = Arrays.asList(data);
        fragmentRecordSettingPara.setValues(list1);
        String[] data2 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "20", "30", "40", "50", "60", "120", "180", "240", "300"};
        List<String> list2 = Arrays.asList(data2);
        fragmentRecordSettingMin.setValues(list2);
    }

    private void initListener() {
        fragmentRecordSettingPara.setmOnSelectListener(new SelectView.onSelect() {
            @Override
            public void onSelectItem(String value) {
                a = Double.parseDouble(value);
                c = b * 60 / a / 30;
                fragmentRecordSettingLength.setText(TimeUtil.secToTime((int) c));
            }
        });
        fragmentRecordSettingMin.setmOnSelectListener(new SelectView.onSelect() {
            @Override
            public void onSelectItem(String value) {
                b = Double.parseDouble(value);
                c = b * 60 / a / 30;
                fragmentRecordSettingLength.setText(TimeUtil.secToTime((int) c));
            }
        });
        fragmentRecordSettingOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RxBus.get().post(Constants.TAG_DIALOG_RECORD_TIME_LAPSE_VALUE, 1 / a);
                RxBus.get().post(Constants.TAG_DIALOG_RECORD_TIME_LAPSE_MAX,(int) c);
                dismiss();
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getDialog().getWindow().setLayout((int) (dm.widthPixels * 0.4), getDialog().getWindow().getAttributes().height);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        RxBus.get().unregister(this);
    }


}
