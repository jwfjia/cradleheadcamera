package com.jiyi.cradleheadcamera.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.hwangjr.rxbus.RxBus;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.common.Command;
import com.jiyi.cradleheadcamera.common.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by IDesigner on 2017/4/24.
 */

public class CradleHeadSettingDialogFragment extends DialogFragment implements View.OnClickListener {


    @Bind(R.id.camera_home)
    ImageView cameraHome;
    @Bind(R.id.camera_camera_setting)
    ImageView cameraCameraSetting;
    @Bind(R.id.camera_cradle_setting)
    ImageView cameraCradleSetting;
    @Bind(R.id.camera_setting)
    ImageView cameraSetting;
    @Bind(R.id.fragment_cradle_setting_gyroscope)
    Button fragmentCradleSettingGyroscope;
    @Bind(R.id.fragment_cradle_setting_accelerometer)
    Button fragmentCradleSettingAccelerometer;
    @Bind(R.id.setting_tracking)
    MaterialSpinner settingTracking;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, 0);
        RxBus.get().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_cradle_setting, container);
        ButterKnife.bind(this, view);
        Log.i("lbw", "===test:onCreateView");
        initSp();
        initListener();
        return view;
    }

    private void initSp() {
        List<Integer> list = new ArrayList<>();
        for (int i = 5; i < 13; i++) {
            list.add(i);
        }
        settingTracking.setItems(list);
        settingTracking.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                RxBus.get().post(Constants.TAG_TRACKING_PARA,(Integer)item);
            }
        });
    }

    private void initListener() {
        fragmentCradleSettingGyroscope.setOnClickListener(this);
        fragmentCradleSettingAccelerometer.setOnClickListener(this);
        cameraHome.setOnClickListener(this);
        cameraCameraSetting.setOnClickListener(this);
        cameraCradleSetting.setOnClickListener(this);
        cameraSetting.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        Log.i("lbw", "===test:onStart");
        super.onStart();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getDialog().setCanceledOnTouchOutside(true);

        Window window = getDialog().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.LEFT;
        lp.x = 0;
        lp.y = -dm.heightPixels;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        window.setLayout((int) (dm.widthPixels * 0.5), getDialog().getWindow().getAttributes().height);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        Log.i("lbw", "===test:onDestroy");
        super.onDestroy();
        RxBus.get().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_camera_setting:
                RxBus.get().post(Constants.TAG_DIALOG_CAMERA_SETTING, "");
                break;
            case R.id.camera_cradle_setting:
                dismiss();
                break;
            case R.id.fragment_cradle_setting_gyroscope:
                RxBus.get().post(Constants.TAG_WRITE, Command.calibration_gyroscope);
                break;
            case R.id.fragment_cradle_setting_accelerometer:
                RxBus.get().post(Constants.TAG_WRITE, Command.calibration_accelerometer);
                break;
        }
    }
}
