package com.jiyi.cradleheadcamera.main.setting.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hwangjr.rxbus.RxBus;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.base.JiyiApplication;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.main.setting.present.SettingPresentImpl;
import com.jiyi.cradleheadcamera.util.SettingUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SettingActivity extends AppCompatActivity implements SettingView, MaterialSpinner.OnItemSelectedListener {

    int cameraId, picListSize, videoListSize;
    SettingPresentImpl settingPresent;
    @Bind(R.id.setting_picture_size)
    MaterialSpinner settingPictureSize;
    @Bind(R.id.setting_video_size)
    MaterialSpinner settingVideoSize;
    @Bind(R.id.setting_iso)
    MaterialSpinner settingIso;
    @Bind(R.id.setting_exposure)
    MaterialSpinner settingExposure;
    @Bind(R.id.setting_tracking)
    MaterialSpinner settingTracking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        RxBus.get().register(this);
        cameraId = getIntent().getIntExtra("cameraid", 0);
        picListSize = getIntent().getIntExtra("piclistsize", 0);
        videoListSize = getIntent().getIntExtra("videolistsize", 0);
        settingPresent = new SettingPresentImpl(this);
        settingPresent.getExposureList();
        settingPresent.getTrackingPara();
        if (JiyiApplication.isoValues != null) {
            settingPresent.getIsoList(JiyiApplication.isoValues);
        }
        if (cameraId == 0) {
            settingPresent.getPhotoSizeList(JiyiApplication.backCameraPictureSizeList);
            settingPresent.getVideoSizeList(JiyiApplication.backCameraVideoSizeList);
        } else {
            settingPresent.getPhotoSizeList(JiyiApplication.frontCameraPictureSizeList);
            settingPresent.getVideoSizeList(JiyiApplication.frontCameraVideoSizeList);
        }

    }

    @Override
    public void initSupportPictureSizeDatas(List<String> list) {
        settingPictureSize.setItems(list);
        settingPictureSize.setOnItemSelectedListener(this);
        settingPresent.getPictureSizePos(cameraId, picListSize);
    }

    @Override
    public void initSupportVideoSizeDatas(List<String> list) {
        settingVideoSize.setItems(list);
        settingVideoSize.setOnItemSelectedListener(this);
        settingPresent.getVideoSizePos(cameraId, videoListSize);
    }

    @Override
    public void initSupportIsoDatas(List<String> list) {
        settingIso.setItems(list);
        settingIso.setOnItemSelectedListener(this);
        settingPresent.getIsoPos(list);
    }

    @Override
    public void initSupportExposureDatas(List<Integer> list) {
        settingExposure.setItems(list);
        settingExposure.setOnItemSelectedListener(this);
        settingPresent.getExposureCompensationPos();
    }

    @Override
    public void initPictureSettingPostion(int pos) {
        settingPictureSize.setSelectedIndex(pos);
    }

    @Override
    public void initVideoSettingPostion(int pos) {
        settingVideoSize.setSelectedIndex(pos);
    }

    @Override
    public void initExposureCompensationPostion(int pos) {
        settingExposure.setSelectedIndex(pos);
    }

    @Override
    public void initIsoPostion(int pos) {
        settingIso.setSelectedIndex(pos);
    }

    @Override
    public void initTrackingPara(List<Integer> list) {
        settingTracking.setItems(list);
        settingTracking.setOnItemSelectedListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        switch (view.getId()) {
            case R.id.setting_picture_size:
                if (cameraId == 0) {
                    SettingUtil.setBackCameraPictureSizePostion(position);
                } else {
                    SettingUtil.setFrontCameraPictureSizePostion(position);
                }
                break;
            case R.id.setting_video_size:
                if (cameraId == 0) {
                    SettingUtil.setBackCameraVideoSizePostion(position);
                } else {
                    SettingUtil.setFrontCameraVideoSizePostion(position);
                }
                break;
            case R.id.setting_exposure:
                SettingUtil.setExposureCompensation((Integer) settingExposure.getItems().get(position));
                break;
            case R.id.setting_iso:
                SettingUtil.setIsoValue((String) settingIso.getItems().get(position));
                break;
            case R.id.setting_tracking:
                RxBus.get().post(Constants.TAG_TRACKING_PARA,(Integer)item);
                break;
        }
    }
}
