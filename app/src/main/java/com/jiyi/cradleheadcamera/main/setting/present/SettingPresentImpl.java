package com.jiyi.cradleheadcamera.main.setting.present;

import android.hardware.Camera;

import com.jiyi.cradleheadcamera.listener.OnCameraParameterParsedListener;
import com.jiyi.cradleheadcamera.main.setting.model.SettingModel;
import com.jiyi.cradleheadcamera.main.setting.model.SettingModelImpl;
import com.jiyi.cradleheadcamera.main.setting.view.SettingView;

import java.util.List;

/**
 * Created by IDesigner on 2017/5/18.
 */

public class SettingPresentImpl implements SettingPresent, OnCameraParameterParsedListener {
    SettingView settingView;
    SettingModel settingModel;

    public SettingPresentImpl(SettingView settingView) {
        this.settingView = settingView;
        settingModel = new SettingModelImpl();
    }


    @Override
    public void getPhotoSizeList(List<Camera.Size> list) {
        settingModel.parsePictureSizeList(this, list);
    }

    @Override
    public void getVideoSizeList(List<Camera.Size> list) {
        settingModel.parseVideoSizeList(this, list);
    }

    @Override
    public void getExposureList() {
        settingView.initSupportExposureDatas(settingModel.getExposureValues());
    }


    @Override
    public void getIsoList(String values) {
        settingModel.parseIsoList(this, values);
    }

    @Override
    public void getPictureSizePos(int cameraId, int size) {
        settingView.initPictureSettingPostion(settingModel.parsePictureSizePostion(cameraId, size));
    }

    @Override
    public void getVideoSizePos(int cameraId, int size) {
        settingView.initVideoSettingPostion(settingModel.parseVideoSizePostion(cameraId, size));
    }

    @Override
    public void getExposureCompensationPos() {
        settingView.initExposureCompensationPostion(settingModel.parseExposureCompensationPostion());
    }

    @Override
    public void getIsoPos(List<String> list) {
        settingView.initIsoPostion(settingModel.parseIsoPostion(list));
    }

    @Override
    public void getTrackingPara() {
        settingView.initTrackingPara(settingModel.getTrackingPara());
    }


    @Override
    public void onPictureSizeParsed(List<String> list) {
        settingView.initSupportPictureSizeDatas(list);
    }

    @Override
    public void onVideoSizeParsed(List<String> list) {
        settingView.initSupportVideoSizeDatas(list);
    }

    @Override
    public void onIsoParsed(List<String> list) {
        settingView.initSupportIsoDatas(list);
    }


}
