package com.jiyi.cradleheadcamera.main.setting.model;

import android.hardware.Camera;
import android.util.Log;

import com.jiyi.cradleheadcamera.listener.OnCameraParameterParsedListener;
import com.jiyi.cradleheadcamera.util.SettingUtil;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by IDesigner on 2017/5/18.
 */

public class SettingModelImpl implements SettingModel {


    @Override
    public void parsePictureSizeList(final OnCameraParameterParsedListener listener, final List<Camera.Size> list) {
        Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(ObservableEmitter<List<String>> e) throws Exception {
                List<String> stringList = new ArrayList<String>();
                for (int i = 0; i < list.size(); i++) {
                    stringList.add(list.get(i).width + "X" + list.get(i).height);
                }
                e.onNext(stringList);
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<String>>() {
                    @Override
                    public void accept(List<String> strings) throws Exception {
                        listener.onPictureSizeParsed(strings);
                    }
                });
    }

    @Override
    public void parseVideoSizeList(final OnCameraParameterParsedListener listener, final List<Camera.Size> list) {
        Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(ObservableEmitter<List<String>> e) throws Exception {
                List<String> stringList = new ArrayList<String>();
                for (int i = 0; i < list.size(); i++) {
                    stringList.add(list.get(i).width + "X" + list.get(i).height);
                }
                e.onNext(stringList);
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<String>>() {
                    @Override
                    public void accept(List<String> strings) throws Exception {
                        listener.onVideoSizeParsed(strings);
                    }
                });
    }

    @Override
    public void parseIsoList(final OnCameraParameterParsedListener listener, final String isoSpeedValues) {
        Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(ObservableEmitter<List<String>> e) throws Exception {
                String[] values = isoSpeedValues.split(",");
                List<String> list = Arrays.asList(values);
                e.onNext(list);
                e.onComplete();
            }
        })
                .observeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<String>>() {
                    @Override
                    public void accept(List<String> strings) throws Exception {
                        listener.onIsoParsed(strings);
                    }
                });
    }

    @Override
    public List<Integer> getExposureValues() {
        List<Integer> list = new ArrayList<>();
        list.add(-3);
        list.add(-2);
        list.add(-1);
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        return list;
    }

    @Override
    public int parsePictureSizePostion(int cameraId, int size) {
        int pos = 0;
        if (cameraId == 0) {
            pos = SettingUtil.getBackCameraPictureSizePostion();
        } else {
            pos = SettingUtil.getFrontCameraPictureSizePostion();
        }
        if (pos == -1) {
            pos = size / 2;
        }
        return pos;
    }

    @Override
    public int parseVideoSizePostion(int cameraId, int size) {
        int pos = 0;
        if (cameraId == 0) {
            pos = SettingUtil.getBackCameraVideoSizePostion();
        } else {
            pos = SettingUtil.getFrontCameraVideoSizePostion();
        }
        if (pos == -1) {
            pos = size / 2;
        }
        return pos;
    }

    @Override
    public int parseExposureCompensationPostion() {
        int values = SettingUtil.getExposureCompensation();
        switch (values) {
            case -3:
                return 0;
            case -2:
                return 1;
            case -1:
                return 2;
            case -0:
                return 3;
            case 1:
                return 4;
            case 2:
                return 5;
            case 3:
                return 6;
        }
        return 0;
    }

    @Override
    public int parseIsoPostion(List<String> list) {
        String value = SettingUtil.getIsoValue();
        int pos = list.indexOf(value);
        return pos;
    }

    @Override
    public List<Integer> getTrackingPara() {
        List<Integer> list = new ArrayList<>();
        for (int i = 5; i < 13; i++) {
            list.add(i);
        }
        return list;
    }
}
