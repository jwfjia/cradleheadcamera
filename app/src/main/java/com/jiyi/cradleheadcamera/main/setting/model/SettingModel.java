package com.jiyi.cradleheadcamera.main.setting.model;

import android.hardware.Camera;

import com.jiyi.cradleheadcamera.listener.OnCameraParameterParsedListener;

import java.util.List;

/**
 * Created by IDesigner on 2017/5/18.
 */

public interface SettingModel {
    void parsePictureSizeList(OnCameraParameterParsedListener listener, List<Camera.Size> list);

    void parseVideoSizeList(OnCameraParameterParsedListener listener, List<Camera.Size> list);

    void parseIsoList(OnCameraParameterParsedListener listener, String isoSpeedValues);

    List<Integer> getExposureValues();

    int parsePictureSizePostion(int cameraId, int size);

    int parseVideoSizePostion(int cameraId, int size);

    int parseExposureCompensationPostion();

    int parseIsoPostion(List<String> list);

    List<Integer> getTrackingPara();
}
