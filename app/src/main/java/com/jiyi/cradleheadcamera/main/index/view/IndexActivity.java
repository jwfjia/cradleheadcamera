package com.jiyi.cradleheadcamera.main.index.view;

import android.Manifest;
import android.bluetooth.BluetoothGatt;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.CameraActivity;
import com.jiyi.cradleheadcamera.FirmwareUpdateActivity;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.common.PermissionList;
import com.jiyi.cradleheadcamera.dialog.ScanBluetoothDeviceDialogFragment;
import com.jiyi.cradleheadcamera.service.BleService;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.EasyPermissions;

public class IndexActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks {

    BleService bleService;
    @Bind(R.id.index_connect_warn)
    TextView indexConnectWarn;
    @Bind(R.id.index_connect)
    Button indexConnect;
    ScanBluetoothDeviceDialogFragment scanDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        initPermission();
        startService(new Intent(getApplicationContext(), BleService.class));
        setContentView(R.layout.activity_index);
        ButterKnife.bind(this);
        RxBus.get().register(this);
        RxBus.get().post(BleService.TAG_REQUEST_SERVICE, "");
        scanDialog = new ScanBluetoothDeviceDialogFragment();
        scanDialog.setCancelable(false);
        indexConnect.setOnClickListener(this);
        findViewById(R.id.osmo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CameraActivity.class));
            }
        });
        indexConnectWarn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), FirmwareUpdateActivity.class));
            }
        });
    }

    private void initPermission() {
        if (!EasyPermissions.hasPermissions(getApplicationContext(), PermissionList.perms)) {
            EasyPermissions.requestPermissions(this, "a", 99, PermissionList.perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
        stopService(new Intent(getApplicationContext(), BleService.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.index_connect:
                scanDialog.show(getSupportFragmentManager(), "scan");
                break;
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_SERVICE_DISCOVERED)})
    public void event(BluetoothGatt gatt) {
        scanDialog.dismiss();
        indexConnectWarn.setText("已连接");
        indexConnect.setText("进入相机");
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(BleService.TAG_SEND_SERVICE)})
    public void event(BleService service) {

        if (bleService == null) {
            bleService = service;
            if (bleService!=null)
            bleService.scanBleDevice();
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        finish();
    }
}
