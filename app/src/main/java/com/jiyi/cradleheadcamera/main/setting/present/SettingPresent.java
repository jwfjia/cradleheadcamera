package com.jiyi.cradleheadcamera.main.setting.present;

import android.hardware.Camera;

import java.util.List;

/**
 * Created by IDesigner on 2017/5/18.
 */

public interface SettingPresent {
    void getPhotoSizeList(List<Camera.Size> list);

    void getVideoSizeList(List<Camera.Size> list);

    void getExposureList();

    void getIsoList(String values);

    void getPictureSizePos(int cameraId, int size);

    void getVideoSizePos(int cameraId, int size);

    void getExposureCompensationPos();

    void getIsoPos(List<String> list);

    void getTrackingPara();
}
