package com.jiyi.cradleheadcamera.main.setting.view;

import java.util.List;

/**
 * Created by IDesigner on 2017/5/18.
 */

public interface SettingView {
    void initSupportPictureSizeDatas(List<String> list);

    void initSupportVideoSizeDatas(List<String> list);

    void initSupportIsoDatas(List<String> list);

    void initSupportExposureDatas(List<Integer> list);

    void initPictureSettingPostion(int pos);

    void initVideoSettingPostion(int pos);

    void initExposureCompensationPostion(int pos);

    void initIsoPostion(int pos);

    void initTrackingPara(List<Integer> list);
}
