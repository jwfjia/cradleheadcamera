package com.jiyi.cradleheadcamera;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.fragment.PhotoColorEditFragment;
import com.jiyi.cradleheadcamera.fragment.PhotoEditBarFragment;
import com.jiyi.cradleheadcamera.fragment.PhotoFilterEditFragment;
import com.jiyi.cradleheadcamera.imagefilter.GPUImageBeautyFilter;
import com.jiyi.cradleheadcamera.imagefilter.IF1977Filter;
import com.jiyi.cradleheadcamera.listener.OnColorSeekbarChangeListener;
import com.jiyi.cradleheadcamera.listener.OnFilterChangeListener;
import com.jiyi.cradleheadcamera.view.MyGpuImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageLookupFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSepiaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;

public class PhotoEditActivity extends AppCompatActivity implements View.OnClickListener, OnColorSeekbarChangeListener, OnFilterChangeListener {
    GPUImageToneCurveFilter curveFilter;
    Bitmap bmp;
    GPUImageBrightnessFilter rawFilter;
    GPUImageFilterGroup filterGroup;
    GPUImageBrightnessFilter brightnessFilter;
    GPUImageContrastFilter contrastFilter;
    GPUImageSaturationFilter saturationFilter;
    GPUImageBeautyFilter beautyFilter;
    //黑白
    GPUImageGrayscaleFilter grayscaleFilter;
    //怀旧
    GPUImageSepiaFilter sepiaFilter;
    //暗角
    GPUImageVignetteFilter vignetteFilter;
    //艳丽
    GPUImageLookupFilter lookupFilter;
    IF1977Filter if1977Filter;
    @Bind(R.id.photo_edit_img)
    MyGpuImageView gpuImageView;
    @Bind(R.id.photo_edit_replace_layout)
    FrameLayout photoEditReplaceLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_edit);
        ButterKnife.bind(this);
        RxBus.get().register(this);
        initFragment();
        initFilter();
        initImage();
        initListener();


    }

    private void initFilter() {
        curveFilter = new GPUImageToneCurveFilter();
        AssetManager as = getAssets();
        try {
            InputStream is = as.open("test.acv");
            curveFilter.setFromCurveFileInputStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if1977Filter = new IF1977Filter(getApplicationContext());
        beautyFilter = new GPUImageBeautyFilter();
        rawFilter = new GPUImageBrightnessFilter();
        brightnessFilter = new GPUImageBrightnessFilter();
        contrastFilter = new GPUImageContrastFilter();
        saturationFilter = new GPUImageSaturationFilter();
        filterGroup = new GPUImageFilterGroup();
        filterGroup.addFilter(brightnessFilter);
        filterGroup.addFilter(contrastFilter);
        filterGroup.addFilter(saturationFilter);
        grayscaleFilter = new GPUImageGrayscaleFilter();
        sepiaFilter = new GPUImageSepiaFilter();
        vignetteFilter = new GPUImageVignetteFilter(new PointF(), new float[]{0.0f, 0.0f, 0.0f}, 0.5f, 1.5f);
        lookupFilter = new GPUImageLookupFilter();
        lookupFilter.setBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.lookup_amatorka));
    }

    private void initFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.photo_edit_replace_layout, new PhotoEditBarFragment()).commit();
    }

    private void initListener() {

    }

    private void initImage() {
        Observable.create(new ObservableOnSubscribe<Bitmap>() {
            @Override
            public void subscribe(ObservableEmitter<Bitmap> e) throws Exception {
                String path = getIntent().getStringExtra("path");
                bmp = BitmapFactory.decodeFile(path);
                e.onNext(bmp);
                e.onComplete();
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Bitmap>() {
                    @Override
                    public void accept(Bitmap bitmap) throws Exception {
                        gpuImageView.setImage(bmp);
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
    }

    void change2Bar() {
        photoEditReplaceLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f));
        getSupportFragmentManager().beginTransaction().replace(R.id.photo_edit_replace_layout, new PhotoEditBarFragment()).commit();
    }

    void change2Color() {
        PhotoColorEditFragment photoColorEditFragment = new PhotoColorEditFragment();
        photoColorEditFragment.setOnColorSeekbarChangeListener(this);
        photoEditReplaceLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.5f));
        getSupportFragmentManager().beginTransaction().replace(R.id.photo_edit_replace_layout, photoColorEditFragment).commit();
    }

    void change2Filter() {
        PhotoFilterEditFragment photoFilterEditFragment = new PhotoFilterEditFragment();
        photoFilterEditFragment.setOnFilterChangeListener(this);
        photoEditReplaceLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.5f));
        getSupportFragmentManager().beginTransaction().replace(R.id.photo_edit_replace_layout, photoFilterEditFragment).commit();
    }

    protected void applyColorFilters() {
        gpuImageView.setFilter(filterGroup);
        gpuImageView.requestRender();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_PHOTO_EDIT_COLOR)})
    public void event(Integer b) {
        change2Color();
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_PHOTO_EDIT_BAR)})
    public void event2(Integer b) {
        change2Bar();
        if (b == 1) {
            gpuImageView.saveImage(Constants.JIYI_PHOTO_DIRECTORY + "temp.jpg");
            gpuImageView.setFilter(rawFilter);
            gpuImageView.getGPUImage().deleteImage();
            gpuImageView.setImage(new File(Constants.JIYI_PHOTO_DIRECTORY + "temp.jpg"));
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_PHOTO_EDIT_FILTER)})
    public void event3(Integer b) {
        change2Filter();
    }

    @Override
    public void onColorChange(int type, float progress) {
        switch (type) {
            case -1:
                rawFilter.setBrightness(0f);
                gpuImageView.setFilter(rawFilter);
                gpuImageView.requestRender();
                break;
            case 0:
                brightnessFilter.setBrightness(progress);
                applyColorFilters();
                break;
            case 1:
                contrastFilter.setContrast(progress);
                applyColorFilters();
                break;
            case 2:
                saturationFilter.setSaturation(progress);
                applyColorFilters();
                break;
        }
    }

    @Override
    public void onFilterChange(int type, float progress) {
        switch (type) {
            case 0:
                rawFilter.setBrightness(0f);
                gpuImageView.setFilter(rawFilter);
                gpuImageView.requestRender();
                break;
            case 1:
                beautyFilter.setBeautyLevel(5.0f);
                gpuImageView.setFilter(beautyFilter);
                gpuImageView.requestRender();
                break;
            case 2:
                gpuImageView.setFilter(grayscaleFilter);
                gpuImageView.requestRender();
                break;
            case 3:
                sepiaFilter.setIntensity(progress);
                gpuImageView.setFilter(sepiaFilter);
                gpuImageView.requestRender();
                break;
            case 4:
                gpuImageView.setFilter(vignetteFilter);
                gpuImageView.requestRender();
                break;
            case 5:
//                lookupFilter.setIntensity(0.2f);
//                gpuImageView.setFilter(lookupFilter);
//                gpuImageView.requestRender();
                gpuImageView.setFilter(curveFilter);
                gpuImageView.requestRender();
                break;
        }
    }
}
