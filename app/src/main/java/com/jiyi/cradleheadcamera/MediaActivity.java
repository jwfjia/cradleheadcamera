package com.jiyi.cradleheadcamera;

import android.app.LocalActivityManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.adapter.MediaViewPagerAdapter;
import com.jiyi.cradleheadcamera.common.Constants;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.activity.VideoPickerActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MediaActivity extends AppCompatActivity {

    MediaViewPagerAdapter adapter;
    LocalActivityManager manager;
    List<String> mTitleList = new ArrayList<>();
    List<View> mViews = new ArrayList<>();
    @Bind(R.id.media_goback)
    ImageView mediaGoback;
    @Bind(R.id.media_tablayout)
    TabLayout mediaTablayout;
    @Bind(R.id.media_viewpager)
    ViewPager mediaViewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        ButterKnife.bind(this);
        RxBus.get().register(this);
        manager = new LocalActivityManager(this, true);
        manager.dispatchCreate(savedInstanceState);
        manager.dispatchDestroy(true);
        Intent photoIntent = new Intent(this, ImagePickerActivity.class);
        photoIntent.putExtra(ImagePickerActivity.INTENT_EXTRA_MODE, ImagePickerActivity.MODE_SINGLE);
        photoIntent.putExtra(ImagePickerActivity.INTENT_EXTRA_TARGET_DIRECTORY, Constants.JIYI_PHOTO_DIRECTORY);
        Intent videoIntent = new Intent(this, VideoPickerActivity.class);
        videoIntent.putExtra(ImagePickerActivity.INTENT_EXTRA_MODE, ImagePickerActivity.MODE_SINGLE);
        videoIntent.putExtra(ImagePickerActivity.INTENT_EXTRA_TARGET_DIRECTORY, Constants.JIYI_VIDEO_DIRECTORY);
        View tab0 = manager.startActivity("0", photoIntent).getDecorView();
        View tab1 = manager.startActivity("1", videoIntent).getDecorView();
        mViews.add(tab0);
        mViews.add(tab1);

        mTitleList.add("照片");
        mTitleList.add("视频");
        mediaTablayout.setTabMode(TabLayout.MODE_FIXED);
        mediaTablayout.addTab(mediaTablayout.newTab().setText(mTitleList.get(0)));
        mediaTablayout.addTab(mediaTablayout.newTab().setText(mTitleList.get(1)));

        adapter = new MediaViewPagerAdapter(mViews, mTitleList);
        mediaViewpager.setAdapter(adapter);
        mediaTablayout.setupWithViewPager(mediaViewpager);
        mediaTablayout.setTabsFromPagerAdapter(adapter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_PHOTO_PICK_PATH)})
    public void event1(String path) {
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setDataAndType(Uri.parse("file://"+path), "image/*");
        Intent intent = new Intent(getApplicationContext(),PhotoPreviewActivity.class);
        intent.putExtra("path",path);
        startActivity(intent);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_VIDEO_PICK_PATH)})
    public void event2(String path) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://"+path), "video/mp4");
        try {
            startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "未找到播发器" + path, Toast.LENGTH_SHORT).show();
        }

        //Toast.makeText(getApplicationContext(), "video" + path, Toast.LENGTH_SHORT).show();
    }
}
