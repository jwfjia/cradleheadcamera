package com.jiyi.cradleheadcamera;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.glomadrian.materialanimatedswitch.MaterialAnimatedSwitch;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.bean.DeviceStatus;
import com.jiyi.cradleheadcamera.common.Command;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.dialog.CameraSettingDialogFragment;
import com.jiyi.cradleheadcamera.dialog.CradleHeadSettingDialogFragment;
import com.jiyi.cradleheadcamera.dialog.ShutterSettingDialogFragment;
import com.jiyi.cradleheadcamera.main.setting.view.SettingActivity;
import com.jiyi.cradleheadcamera.thread.TimeCountThread;
import com.jiyi.cradleheadcamera.util.Codeutil;
import com.jiyi.cradleheadcamera.util.NdkLoader;
import com.jiyi.cradleheadcamera.util.SettingUtil;
import com.jiyi.cradleheadcamera.util.TimeUtil;
import com.jiyi.cradleheadcamera.view.CameraGirdView;
import com.jiyi.cradleheadcamera.view.CameraView;
import com.jiyi.cradleheadcamera.view.DrawRectView;
import com.jiyi.cradleheadcamera.view.FocusView;
import com.jiyi.cradleheadcamera.view.MyCountdownView;
import com.jiyi.cradleheadcamera.view.VerticalSeekBar;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class CameraActivity extends AppCompatActivity implements View.OnClickListener, CameraView.OnCameraStateChangedListener {
    List<String> panoPathList = new ArrayList<>();
    TimeCountThread timeCountThread;
    boolean isTracking = false;
    boolean isPanoMode = false;
    boolean panoReady = false;
    boolean isPanoing = false;
    int delayTake = 0;
    AlphaAnimation alphaAnimation1;
    boolean isPortrait = false;
    OrientationEventListener mScreenOrientationEventListener;
    CradleHeadSettingDialogFragment cradleHeadSettingDialogFragment;
    CameraSettingDialogFragment cameraSettingDialogFragment;
    ShutterSettingDialogFragment shutterSettingDialogFragment;
    @Bind(R.id.camera_home)
    ImageView cameraHome;
    @Bind(R.id.camera_camera_setting)
    ImageView cameraCameraSetting;
    @Bind(R.id.camera_cradle_setting)
    ImageView cameraCradleSetting;
    @Bind(R.id.camera_setting)
    ImageView cameraSetting;
    @Bind(R.id.camera_photo_mode)
    ImageView cameraPhotoMode;
    @Bind(R.id.camera_shutter)
    ImageView cameraShutter;
    @Bind(R.id.camera_camera_switch)
    ImageView cameraCameraSwitch;
    @Bind(R.id.camera_photo_display)
    ImageView cameraPhotoDisplay;
    @Bind(R.id.camera_mode_switch)
    MaterialAnimatedSwitch cameraModeSwitch;
    @Bind(R.id.camera_cameraview)
    CameraView cameraCameraview;
    @Bind(R.id.camera_gird)
    CameraGirdView cameraGird;
    @Bind(R.id.camera_count_down_view)
    MyCountdownView cameraCountDownView;
    @Bind(R.id.camera_device_status)
    TextView cameraDeviceStatus;
    @Bind(R.id.camera_drawrectview)
    DrawRectView cameraDrawrectview;
    @Bind(R.id.camera_tracking)
    ImageView cameraTracking;
    @Bind(R.id.linearLayout)
    LinearLayout linearLayout;
    @Bind(R.id.camera_record_time)
    TextView cameraRecordTime;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            cameraRecordTime.setText(TimeUtil.secToTime(msg.what));
        }
    };
    @Bind(R.id.camera_pano9_status)
    TextView cameraPano9Status;
    @Bind(R.id.camera_exposure_seekbar)
    VerticalSeekBar cameraExposureSeekbar;
    @Bind(R.id.camera_focusview)
    FocusView cameraFocusview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);
        RxBus.get().register(this);
        initDialog();
        initListener();
        initAnimation();
        NdkLoader.nativeCreateObject();
        RxBus.get().post(Constants.TAG_WRITE, Command.request_device_status);
    }

    private void initCameraSetting() {
        if (cameraCameraview.cameraPosition == 0) {
            int currentBackPicPos = SettingUtil.getBackCameraPictureSizePostion();
            int currentBackVideoPos = SettingUtil.getBackCameraVideoSizePostion();
            cameraCameraview.setPictureSize(currentBackPicPos);
            cameraCameraview.setVideoSize(currentBackVideoPos);
        } else {
            int currentFrontPicPos = SettingUtil.getFrontCameraPictureSizePostion();
            int currentFrontVideoPos = SettingUtil.getFrontCameraVideoSizePostion();
            cameraCameraview.setPictureSize(currentFrontPicPos);
            cameraCameraview.setVideoSize(currentFrontVideoPos);
        }
        int currentExposureCompensation = SettingUtil.getExposureCompensation();
        cameraCameraview.setExposureCompensation(currentExposureCompensation);
        String currentIso = SettingUtil.getIsoValue();
        cameraCameraview.setIso(currentIso);
    }

    private void initAnimation() {
        if (alphaAnimation1 == null) {
            alphaAnimation1 = new AlphaAnimation(0.1f, 0.4f);
        }
        alphaAnimation1.setDuration(5000);
    }

    private void initDialog() {
        cradleHeadSettingDialogFragment = new CradleHeadSettingDialogFragment();
        cameraSettingDialogFragment = new CameraSettingDialogFragment();
        shutterSettingDialogFragment = new ShutterSettingDialogFragment();
    }

    private void initListener() {
        cameraFocusview.setOnFocusViewChangedListener(new FocusView.OnFocusViewChangedListener() {
            @Override
            public void onVisible() {
                cameraExposureSeekbar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onGone() {
                cameraExposureSeekbar.setVisibility(View.GONE);
            }
        });
        cameraExposureSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                cameraFocusview.updateDownCount();
                cameraFocusview.startNewThread();
                cameraCameraview.setExposureCompensation(progress - 3);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        cameraDrawrectview.setOnTouchScreenListener(new DrawRectView.OnTouchScreenListener() {
            @Override
            public void onUp(int ax, int ay, int bx, int by) {
                isTracking = true;
                NdkLoader.bbox(ax, ay, bx, by);
            }

            @Override
            public void onDown() {
                isTracking = false;
            }
        });
        cameraCameraview.setOnCameraStateChangedListener(this);
        cameraHome.setOnClickListener(this);
        cameraCameraSetting.setOnClickListener(this);
        cameraCradleSetting.setOnClickListener(this);
        cameraSetting.setOnClickListener(this);
        cameraPhotoMode.setOnClickListener(this);
        cameraShutter.setOnClickListener(this);
        cameraCameraSwitch.setOnClickListener(this);
        cameraPhotoDisplay.setOnClickListener(this);
        cameraTracking.setOnClickListener(this);

        cameraCountDownView.setOnFinishAction(new MyCountdownView.Action() {
            @Override
            public void onAction() {
                cameraCountDownView.setVisibility(View.INVISIBLE);
            }
        });

        cameraModeSwitch.setOnCheckedChangeListener(new MaterialAnimatedSwitch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(boolean b) {
                if (b) {
                    cameraShutter.setTag("record");
                    cameraShutter.setImageResource(R.drawable.btn_camera_camera_record_background);
                } else {
                    cameraShutter.setTag("photo");
                    cameraShutter.setImageResource(R.drawable.btn_camera_camera_shutter_background);
                }
            }
        });

        mScreenOrientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int orientation) {
                if (orientation == OrientationEventListener.ORIENTATION_UNKNOWN) {
                    return;
                }
                if ((orientation > 350 && orientation < 360) || (orientation > 0 && orientation < 10)) {
                    isPortrait = true;
                    if (cameraHome != null) {
                        cameraModeSwitch.setRotation(-90);
                        cameraHome.setRotation(-90);
                        cameraCameraSetting.setRotation(-90);
                        cameraCradleSetting.setRotation(-90);
                        cameraSetting.setRotation(-90);
                        cameraPhotoMode.setRotation(-90);
                        cameraCameraSwitch.setRotation(-90);
                        cameraPhotoDisplay.setRotation(-90);
                    }
                }
                if (orientation > 260 && orientation < 280) {
                    isPortrait = false;
                    if (cameraHome != null) {
                        cameraModeSwitch.setRotation(0);
                        cameraHome.setRotation(0);
                        cameraCameraSetting.setRotation(0);
                        cameraCradleSetting.setRotation(0);
                        cameraSetting.setRotation(0);
                        cameraPhotoMode.setRotation(0);
                        cameraCameraSwitch.setRotation(0);
                        cameraPhotoDisplay.setRotation(0);
                    }
                }
                if (orientation > 80 && orientation < 100) {
                    isPortrait = false;
                    if (cameraHome != null) {
                        cameraModeSwitch.setRotation(0);
                        cameraHome.setRotation(0);
                        cameraCameraSetting.setRotation(0);
                        cameraCradleSetting.setRotation(0);
                        cameraSetting.setRotation(0);
                        cameraPhotoMode.setRotation(0);
                        cameraCameraSwitch.setRotation(0);
                        cameraPhotoDisplay.setRotation(0);
                    }
                }
                if (orientation > 170 && orientation < 190) {
                    isPortrait = true;
                    if (cameraHome != null) {
                        cameraModeSwitch.setRotation(-90);
                        cameraHome.setRotation(-90);
                        cameraCameraSetting.setRotation(-90);
                        cameraCradleSetting.setRotation(-90);
                        cameraSetting.setRotation(-90);
                        cameraPhotoMode.setRotation(-90);
                        cameraCameraSwitch.setRotation(-90);
                        cameraPhotoDisplay.setRotation(-90);
                    }
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_cradle_setting:
                if (shutterSettingDialogFragment.isVisible()) {
                    shutterSettingDialogFragment.dismiss();
                }
                if (cameraSettingDialogFragment.isVisible()) {
                    cameraSettingDialogFragment.dismiss();
                }
                if (!cradleHeadSettingDialogFragment.isVisible()) {
                    cradleHeadSettingDialogFragment.show(getSupportFragmentManager(), "1");
                }
                break;
            case R.id.camera_camera_setting:
                if (shutterSettingDialogFragment.isVisible()) {
                    shutterSettingDialogFragment.dismiss();
                }
                if (cradleHeadSettingDialogFragment.isVisible()) {
                    cradleHeadSettingDialogFragment.dismiss();
                }
                if (!cameraSettingDialogFragment.isVisible()) {
                    cameraSettingDialogFragment.show(getSupportFragmentManager(), "2");
                }
                break;
            case R.id.camera_photo_mode:
                if (cameraSettingDialogFragment.isVisible()) {
                    cameraSettingDialogFragment.dismiss();
                }
                if (cradleHeadSettingDialogFragment.isVisible()) {
                    cradleHeadSettingDialogFragment.dismiss();
                }
                if (!shutterSettingDialogFragment.isVisible()) {
                    shutterSettingDialogFragment.show(getSupportFragmentManager(), "3");
                }
                break;
            case R.id.camera_shutter:
                takeShutter();
                break;
            case R.id.camera_camera_switch:
                cameraCameraSwitch.setEnabled(false);
                cameraCameraview.changeCamera();
                initCameraSetting();
                break;
            case R.id.camera_photo_display:
                startActivity(new Intent(getApplicationContext(), MediaActivity.class));
                break;
            case R.id.camera_home:
                finish();
                break;
            case R.id.camera_setting:
                Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
                intent.putExtra("cameraid", cameraCameraview.cameraPosition);
                intent.putExtra("piclistsize", cameraCameraview.getPictureListSize());
                intent.putExtra("videolistsize", cameraCameraview.getVideoListSize());
                startActivity(intent);
                break;
            case R.id.camera_tracking:
                if (v.getTag().equals("off")) {
                    v.setTag("on");
                    cameraTracking.setImageResource(R.mipmap.longan_camera_centerpoints_bule2);
                    cameraDrawrectview.setVisibility(View.VISIBLE);
                    RxBus.get().post(Constants.TAG_WRITE, Command.tracking_start);
                } else {
                    v.setTag("off");
                    cameraTracking.setImageResource(R.mipmap.longan_camera_centerpoints2);
                    cameraDrawrectview.setVisibility(View.GONE);
                    NdkLoader.endruning();
                    isTracking = false;
                    cameraDrawrectview.updateRectPostion(0, 0, 0, 0);
                }
                break;
        }
    }

    public void takeShutter() {

        if (cameraShutter.getTag().equals("photo")) {
            cameraShutter.setEnabled(false);
            if (cameraModeSwitch.isChecked()) {
                cameraModeSwitch.toggle();
            }
            if (delayTake != 0) {
                cameraCountDownView.setVisibility(View.VISIBLE);
                cameraCountDownView.setTime(delayTake);
                cameraCountDownView.star();
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (alphaAnimation1 != null) {
                        alphaAnimation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                cameraCameraview.takeOneJpgPhoto(Constants.JIYI_PHOTO_DIRECTORY, isPortrait);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        cameraCameraview.startAnimation(alphaAnimation1);
                    }

                }
            }, delayTake);


        } else if (cameraShutter.getTag().equals("record")) {

            if (cameraCameraview.isRecording) {
                if (isPanoing) {
                    return;
                }
                Toast.makeText(getApplicationContext(), "stop", Toast.LENGTH_SHORT).show();
                cameraCameraview.stopRecord();

                mScreenOrientationEventListener.enable();
            } else {
                if (isPanoMode) {
                    if (panoReady) {
                        isPanoing = true;
                        RxBus.get().post(Constants.TAG_WRITE, Command.request_pano_start);
                        if (!cameraModeSwitch.isChecked()) {
                            cameraModeSwitch.toggle();
                        }
                        mScreenOrientationEventListener.disable();
                        if (isPortrait) {
                            cameraModeSwitch.setRotation(0);
                            cameraHome.setRotation(0);
                            cameraCameraSetting.setRotation(0);
                            cameraCradleSetting.setRotation(0);
                            cameraSetting.setRotation(0);
                            cameraPhotoMode.setRotation(0);
                            cameraCameraSwitch.setRotation(0);
                            cameraPhotoDisplay.setRotation(0);
                        }
                        Toast.makeText(getApplicationContext(), "start", Toast.LENGTH_SHORT).show();
                        cameraCameraview.startRecord(Constants.JIYI_VIDEO_DIRECTORY, isPortrait);
                    } else {
                        RxBus.get().post(Constants.TAG_WRITE, Command.request_pano_isready);
                    }
                } else {
                    if (!cameraModeSwitch.isChecked()) {
                        cameraModeSwitch.toggle();
                    }
                    mScreenOrientationEventListener.disable();
                    if (isPortrait) {
                        cameraModeSwitch.setRotation(0);
                        cameraHome.setRotation(0);
                        cameraCameraSetting.setRotation(0);
                        cameraCradleSetting.setRotation(0);
                        cameraSetting.setRotation(0);
                        cameraPhotoMode.setRotation(0);
                        cameraCameraSwitch.setRotation(0);
                        cameraPhotoDisplay.setRotation(0);
                    }
                    Toast.makeText(getApplicationContext(), "start", Toast.LENGTH_SHORT).show();
                    cameraCameraview.startRecord(Constants.JIYI_VIDEO_DIRECTORY, isPortrait);
                }

            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (cameraCameraview != null) {
            cameraCameraview.initCamera();
            initCameraSetting();
        }
        if (mScreenOrientationEventListener != null) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    mScreenOrientationEventListener.enable();
                }
            }, 300);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (cameraCameraview != null) {
            cameraCameraview.releaseCamera();
        }
        if (mScreenOrientationEventListener != null) {
            mScreenOrientationEventListener.disable();
        }
        NdkLoader.release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
    }

    //cameraview callback
    @Override
    public void onPhotoSaved(String filePath) {
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(new File(filePath));
        scanIntent.setData(contentUri);
        sendBroadcast(scanIntent);
        cameraShutter.setEnabled(true);
        cameraCameraview.clearAnimation();
        if (cameraPano9Status.getVisibility() == View.VISIBLE) {
            panoPathList.add(filePath);
            cameraPano9Status.setText("九宫格全景已开启：" + panoPathList.size() + "/9");
            if (panoPathList.size() == 9) {
                Intent intentPut = new Intent(getApplicationContext(), PhotoPreviewActivity.class);
                intentPut.putExtra("panoPathList", (Serializable) panoPathList);
                startActivity(intentPut);
                panoPathList.clear();
                cameraPano9Status.setText("九宫格全景已开启：" + panoPathList.size() + "/9");
            }
        } else {
            panoPathList.clear();
        }
    }

    @Override
    public void onVideoSaved(String filePath) {
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(new File(filePath));
        scanIntent.setData(contentUri);
        sendBroadcast(scanIntent);
    }

    @Override
    public void onCameraChanged() {
        cameraCameraSwitch.setEnabled(true);
    }

    @Override
    public void onCurrentPreviewFrame(final byte[] frame) {
        if (isTracking) {
            long startTime = System.currentTimeMillis();
            int[] aa = NdkLoader.salt(frame, cameraCameraview.previewHeight, cameraCameraview.previewWidth, cameraCameraview.cameraPosition);
            long endTime = System.currentTimeMillis();

            moveRect(aa);
            float seconds = (endTime - startTime) / 1000F;
            Log.i("lbw","===time:"+seconds);
//            Observable.create(new ObservableOnSubscribe<int[]>() {
//                @Override
//                public void subscribe(ObservableEmitter<int[]> e) throws Exception {
//                    int[] aa = NdkLoader.salt(frame, cameraCameraview.previewHeight, cameraCameraview.previewWidth, cameraCameraview.cameraPosition);
//                    e.onNext(aa);
//                    e.onComplete();
//                }
//            })
//                    .subscribeOn(Schedulers.computation())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(new Consumer<int[]>() {
//                        @Override
//                        public void accept(int[] ints) throws Exception {
//                            moveRect(ints);
//                        }
//                    });
        }
    }

    private void moveRect(int[] aa) {

        int[] bb = new int[5];
        bb[0] = aa[0] + aa[2] / 2;
        bb[1] = aa[1] + aa[3] / 2;

//                cameraDrawrectview.updateRectPostion(aa[0], aa[1], aa[0] + aa[2], aa[1] + aa[2]);
        if (aa[4] == 1) {
            cameraDrawrectview.updateRectPostion(aa[0] - aa[2] / 2, aa[1] - aa[3] / 2, aa[0] + aa[2] / 2, aa[1] + aa[3] / 2);
        } else {
            cameraDrawrectview.updateRectPostion(0, 0, 0, 0);
        }
        RxBus.get().post(Constants.TAG_WRITE, Codeutil.convertCoordinate(aa, cameraCameraview.previewWidth, cameraCameraview.previewHeight, cameraCameraview.cameraPosition));


    }


    //RXBUS
    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_CAMERA_SETTING)})
    public void event1(String s) {
        cameraCameraSetting.performClick();
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_CRADLE_SETTING)})
    public void event2(String s) {
        cameraCradleSetting.performClick();
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_SETTING)})
    public void event3(String s) {
        cameraSetting.performClick();
    }

    //接收拍照指令
    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_CAMERA_SHUTTER)})
    public void event4(String s) {
        cameraShutter.setTag("photo");
        takeShutter();
    }

    //接收录像指令
    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_CAMERA_RECORD)})
    public void event5(String s) {
        cameraShutter.setTag("record");
        takeShutter();
    }

    //变焦指令
    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_CAMERA_ZOOM)})
    public void event6(Boolean b) {
        cameraCameraview.setZoom(b);
    }


    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_CAMERA_CHANGE_WHITE_BALANCE)})
    public void event7(String s) {
        cameraCameraview.setWhiteBalanceValue(s);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_CAMERA_CHANGE_CAMERA_GRID)})
    public void event8(Integer i) {
        cameraGird.setGridType(i);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_CAMERA_CHANGE_CAMERA_FLASH)})
    public void event9(String type) {
        cameraCameraview.setFlashLightMode(type);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_FRAGMENT_SHUTTER_SETTING_CHANGE)})
    public void event10(Integer id) {
        cameraPhotoMode.setImageResource(id);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_CAMERA_SHUTTER_DELAY)})
    public void event11(Integer time) {
        delayTake = time;
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DEVICE_STATUS)})
    public void event11(DeviceStatus status) {
        cameraDeviceStatus.setText("固件版本：" + status.getFirmwareVersion() + " " + "状态：" + status.getWorkingStatus() + " " + "模式：" + status.getWorkingMode() + " 红外传感器状态：" + status.getProximity());
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_PANO_READY)})
    public void event12(Integer i) {
        panoReady = true;
        takeShutter();
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_PANO_END)})
    public void event13(Integer i) {
        panoReady = false;
        isPanoing = false;
        takeShutter();
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_PANO_MODE)})
    public void event14(Boolean b) {
        isPanoMode = b;
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_RECORD_TIME_LAPSE_MODE)})
    public void event15(Boolean b) {
        cameraCameraview.setTimeLapseMode(b);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_RECORD_TIME_LAPSE_VALUE)})
    public void event16(Double d) {
        cameraCameraview.setTimeLapseValue(d);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_DIALOG_RECORD_TIME_LAPSE_MAX)})
    public void event17(Integer d) {
        cameraCameraview.setMaxTime(d * 1000);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_START_TIME_COUNT)})
    public void event18(Boolean b) {
        if (b) {
            handler.sendEmptyMessage(0);
            cameraRecordTime.setVisibility(View.VISIBLE);
            if (timeCountThread != null) {
                timeCountThread.setFlag(false);
                timeCountThread = null;
                timeCountThread = new TimeCountThread(handler);
                timeCountThread.start();
            } else {
                timeCountThread = new TimeCountThread(handler);
                timeCountThread.start();
            }
        } else {
            cameraRecordTime.setVisibility(View.GONE);
            if (timeCountThread != null) {
                timeCountThread.setFlag(false);
            }
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_PANO9)})
    public void event19(Boolean b) {
        if (b) {
            cameraCameraview.setPictureSize(1024, 768);
            cameraPano9Status.setVisibility(View.VISIBLE);
        } else {
            initCameraSetting();
            cameraPano9Status.setVisibility(View.GONE);
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_TRACKING_PARA)})
    public void event20(Integer d) {
        NdkLoader.Threshold(d);
    }
}
