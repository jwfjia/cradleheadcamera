package com.jiyi.cradleheadcamera.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.base.JiyiApplication;
import com.jiyi.cradleheadcamera.common.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CameraSettingGridFragment extends Fragment implements View.OnClickListener {


    @Bind(R.id.fragment_grid_none)
    RelativeLayout fragmentGridNone;
    @Bind(R.id.fragment_grid_well)
    RelativeLayout fragmentGridWell;
    @Bind(R.id.fragment_grid_diagonal)
    RelativeLayout fragmentGridDiagonal;
    @Bind(R.id.fragment_grid_center)
    RelativeLayout fragmentGridCenter;
    @Bind(R.id.fragment_grid_layout)
    LinearLayout fragmentGridLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_setting_grid, container, false);
        ButterKnife.bind(this, view);
        RxBus.get().register(this);
        fragmentGridNone.setOnClickListener(this);
        fragmentGridWell.setOnClickListener(this);
        fragmentGridDiagonal.setOnClickListener(this);
        fragmentGridCenter.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        int id = JiyiApplication.setting.get("grid");
        select(id);
    }

    public void select(int id) {
        for (int i = 0; i < fragmentGridLayout.getChildCount(); i++) {
            View v = fragmentGridLayout.getChildAt(i);
            if (v instanceof RelativeLayout) {
                for (int j = 0; j < ((RelativeLayout) v).getChildCount(); j++) {
                    View v2 = ((RelativeLayout) v).getChildAt(j);
                    if (v2 instanceof ImageView) {
                        if (v.getId() == id) {
                            v2.setVisibility(View.VISIBLE);
                        } else {
                            v2.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        RxBus.get().unregister(this);
    }

    @Override
    public void onClick(View v) {
        JiyiApplication.setting.put("grid",v.getId());
        switch (v.getId()) {
            case R.id.fragment_grid_none:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_CAMERA_GRID, 0);
                break;
            case R.id.fragment_grid_well:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_CAMERA_GRID, 1);
                break;
            case R.id.fragment_grid_diagonal:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_CAMERA_GRID, 2);
                break;
            case R.id.fragment_grid_center:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_CAMERA_GRID, 3);
                break;
        }
        select(v.getId());
    }
}
