package com.jiyi.cradleheadcamera.fragment;


import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.listener.OnFilterChangeListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoFilterEditFragment extends Fragment implements View.OnClickListener {


    OnFilterChangeListener onFilterChangeListener;

    @Bind(R.id.photo_edit_filter_cancel)
    ImageView photoEditFilterCancel;
    @Bind(R.id.photo_edit_filter_ok)
    ImageView photoEditFilterOk;
    @Bind(R.id.photo_edit_filter_raw)
    RadioButton photoEditFilterRaw;
    @Bind(R.id.photo_edit_filter_beauty)
    RadioButton photoEditFilterBeauty;
    @Bind(R.id.photo_edit_filter_black_white)
    RadioButton photoEditFilterBlackWhite;
    @Bind(R.id.photo_edit_filter_old)
    RadioButton photoEditFilterOld;
    @Bind(R.id.photo_edit_filter_dark)
    RadioButton photoEditFilterDark;
    @Bind(R.id.photo_edit_filter_gorgeous)
    RadioButton photoEditFilterGorgeous;
    @Bind(R.id.photo_edit_filter_radiogroup)
    RadioGroup photoEditFilterRadiogroup;

    public void setOnFilterChangeListener(OnFilterChangeListener onFilterChangeListener) {
        this.onFilterChangeListener = onFilterChangeListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_filter_edit, container, false);
        ButterKnife.bind(this, view);
        initListener();
        return view;
    }

    private void initListener() {
        photoEditFilterRadiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.photo_edit_filter_raw:
                        onFilterChangeListener.onFilterChange(0, 0);
                        break;
                    case R.id.photo_edit_filter_beauty:
                        onFilterChangeListener.onFilterChange(1, 1.5f);
                        break;
                    case R.id.photo_edit_filter_black_white:
                        onFilterChangeListener.onFilterChange(2, 0);
                        break;
                    case R.id.photo_edit_filter_old:
                        onFilterChangeListener.onFilterChange(3, 0.6f);
                        break;
                    case R.id.photo_edit_filter_dark:
                        onFilterChangeListener.onFilterChange(4, 0.25f);
                        break;
                    case R.id.photo_edit_filter_gorgeous:
                        onFilterChangeListener.onFilterChange(5, 0);
                        break;
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photo_edit_filter_raw:
                onFilterChangeListener.onFilterChange(0, 0);
                break;
            case R.id.photo_edit_filter_beauty:
                onFilterChangeListener.onFilterChange(1, 1.5f);
                break;
        }
    }
}
