package com.jiyi.cradleheadcamera.fragment;


import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.base.JiyiApplication;
import com.jiyi.cradleheadcamera.common.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CameraSettingWhiteBalanceFragment extends Fragment implements View.OnClickListener {


    @Bind(R.id.fragment_white_balance_auto)
    RelativeLayout fragmentWhiteBalanceAuto;
    @Bind(R.id.fragment_white_balance_incandescent)
    RelativeLayout fragmentWhiteBalanceIncandescent;
    @Bind(R.id.fragment_white_balance_fluorescent)
    RelativeLayout fragmentWhiteBalanceFluorescent;
    @Bind(R.id.fragment_white_balance_daylight)
    RelativeLayout fragmentWhiteBalanceDaylight;
    @Bind(R.id.fragment_white_balance_cloudydaylight)
    RelativeLayout fragmentWhiteBalanceCloudydaylight;
    @Bind(R.id.fragment_white_balance_shade)
    RelativeLayout fragmentWhiteBalanceShade;
    @Bind(R.id.fragment_white_balance_twilight)
    RelativeLayout fragmentWhiteBalanceTwilight;
    @Bind(R.id.fragment_white_balance_layout)
    LinearLayout fragmentWhiteBalanceLayout;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_setting_white_balance, container, false);
        ButterKnife.bind(this, view);
        RxBus.get().register(this);
        fragmentWhiteBalanceAuto.setOnClickListener(this);
        fragmentWhiteBalanceIncandescent.setOnClickListener(this);
        fragmentWhiteBalanceFluorescent.setOnClickListener(this);
        fragmentWhiteBalanceDaylight.setOnClickListener(this);
        fragmentWhiteBalanceCloudydaylight.setOnClickListener(this);
        fragmentWhiteBalanceShade.setOnClickListener(this);
        fragmentWhiteBalanceTwilight.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        int id = JiyiApplication.setting.get("whitebalance");
        select(id);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        RxBus.get().unregister(this);
    }

    public void select(int id) {
        for (int i = 0; i < fragmentWhiteBalanceLayout.getChildCount(); i++) {
            View v = fragmentWhiteBalanceLayout.getChildAt(i);
            if (v instanceof RelativeLayout) {
                for (int j = 0; j < ((RelativeLayout) v).getChildCount(); j++) {
                    View v2 = ((RelativeLayout) v).getChildAt(j);
                    if (v2 instanceof ImageView){
                        if (v.getId() == id) {
                            v2.setVisibility(View.VISIBLE);
                        } else {
                            v2.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onClick(View v) {
        JiyiApplication.setting.put("whitebalance",v.getId());
        switch (v.getId()){
            case R.id.fragment_white_balance_auto:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_WHITE_BALANCE, Camera.Parameters.WHITE_BALANCE_AUTO);
                break;
            case R.id.fragment_white_balance_incandescent:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_WHITE_BALANCE,Camera.Parameters.WHITE_BALANCE_INCANDESCENT);
                break;
            case R.id.fragment_white_balance_fluorescent:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_WHITE_BALANCE,Camera.Parameters.WHITE_BALANCE_FLUORESCENT);
                break;
            case R.id.fragment_white_balance_daylight:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_WHITE_BALANCE,Camera.Parameters.WHITE_BALANCE_DAYLIGHT);
                break;
            case R.id.fragment_white_balance_cloudydaylight:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_WHITE_BALANCE,Camera.Parameters.WHITE_BALANCE_CLOUDY_DAYLIGHT);
                break;
            case R.id.fragment_white_balance_shade:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_WHITE_BALANCE,Camera.Parameters.WHITE_BALANCE_SHADE);
                break;
            case R.id.fragment_white_balance_twilight:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_WHITE_BALANCE,Camera.Parameters.WHITE_BALANCE_TWILIGHT);
                break;
        }
        select(v.getId());
    }
}
