package com.jiyi.cradleheadcamera.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.common.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CameraSettingIndexFragment extends Fragment implements View.OnClickListener {


    @Bind(R.id.fragment_camera_setting_white_balance)
    RelativeLayout fragmentCameraSettingWhiteBalance;
    @Bind(R.id.fragment_camera_setting_flash)
    RelativeLayout fragmentCameraSettingFlash;
    @Bind(R.id.fragment_camera_setting_grid)
    RelativeLayout fragmentCameraSettingGrid;

    public CameraSettingIndexFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RxBus.get().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_setting_index, container, false);
        ButterKnife.bind(this, view);
        fragmentCameraSettingWhiteBalance.setOnClickListener(this);
        fragmentCameraSettingFlash.setOnClickListener(this);
        fragmentCameraSettingGrid.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        RxBus.get().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_camera_setting_white_balance:
                RxBus.get().post(Constants.TAG_FRAGMENT_CAMERA_SETTING,1);
                break;
            case R.id.fragment_camera_setting_flash:
                RxBus.get().post(Constants.TAG_FRAGMENT_CAMERA_SETTING,2);
                break;
            case R.id.fragment_camera_setting_grid:
                RxBus.get().post(Constants.TAG_FRAGMENT_CAMERA_SETTING,3);
                break;
        }
    }
}
