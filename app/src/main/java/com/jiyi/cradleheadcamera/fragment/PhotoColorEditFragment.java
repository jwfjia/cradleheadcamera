package com.jiyi.cradleheadcamera.fragment;


import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.listener.OnColorSeekbarChangeListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoColorEditFragment extends Fragment implements View.OnClickListener {

    OnColorSeekbarChangeListener onColorSeekbarChangeListener;
    int brightbnessProgress = 50;
    int contrastProgress = 50;
    int saturationProgress = 50;
    @Bind(R.id.photo_edit_color_brightness)
    RadioButton photoEditColorBrightness;
    @Bind(R.id.photo_edit_color_contrast)
    RadioButton photoEditColorContrast;
    @Bind(R.id.photo_edit_color_saturation)
    RadioButton photoEditColorSaturation;
    @Bind(R.id.photo_edit_color_radiogroup)
    RadioGroup photoEditColorRadiogroup;
    @Bind(R.id.photo_edit_color_cancel)
    ImageView photoEditColorCancel;
    @Bind(R.id.photo_edit_color_seekbar)
    SeekBar photoEditColorSeekbar;
    @Bind(R.id.photo_edit_color_ok)
    ImageView photoEditColorOk;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_color_edit, container, false);
        ButterKnife.bind(this, view);
        RxBus.get().register(this);
        initListener();
        return view;
    }

    private void initListener() {
        photoEditColorOk.setOnClickListener(this);
        photoEditColorCancel.setOnClickListener(this);
        photoEditColorSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    switch (photoEditColorRadiogroup.getCheckedRadioButtonId()) {
                        case R.id.photo_edit_color_brightness:
                            brightbnessProgress = progress;
                            if (onColorSeekbarChangeListener != null) {
                                onColorSeekbarChangeListener.onColorChange(0, range(progress, -1.0f, 1f));
                            }
                            break;
                        case R.id.photo_edit_color_contrast:
                            contrastProgress = progress;
                            if (onColorSeekbarChangeListener != null) {
                                onColorSeekbarChangeListener.onColorChange(1, range(progress, 0f, 4.0f));
                            }
                            break;
                        case R.id.photo_edit_color_saturation:
                            saturationProgress = progress;
                            if (onColorSeekbarChangeListener != null) {
                                onColorSeekbarChangeListener.onColorChange(2, range(progress, 0f, 2f));
                            }
                            break;
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        photoEditColorRadiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.photo_edit_color_brightness:
                        photoEditColorSeekbar.setProgress(brightbnessProgress);
                        break;
                    case R.id.photo_edit_color_contrast:
                        photoEditColorSeekbar.setProgress(contrastProgress);
                        break;
                    case R.id.photo_edit_color_saturation:
                        photoEditColorSeekbar.setProgress(saturationProgress);
                        break;
                }
            }
        });
    }

    protected float range(final int percentage, final float start, final float end) {

        return (end - start) * percentage / 100.0f + start;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        RxBus.get().unregister(this);
    }

    public void setOnColorSeekbarChangeListener(OnColorSeekbarChangeListener onColorSeekbarChangeListener) {
        this.onColorSeekbarChangeListener = onColorSeekbarChangeListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photo_edit_color_ok:
                RxBus.get().post(Constants.TAG_PHOTO_EDIT_BAR, 1);
                break;
            case R.id.photo_edit_color_cancel:
                RxBus.get().post(Constants.TAG_PHOTO_EDIT_BAR, 0);
                if (onColorSeekbarChangeListener != null) {
                    onColorSeekbarChangeListener.onColorChange(-1, 0);
                }
                break;
        }
    }
}
