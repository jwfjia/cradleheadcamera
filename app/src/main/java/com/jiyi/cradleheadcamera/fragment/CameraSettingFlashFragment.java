package com.jiyi.cradleheadcamera.fragment;


import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.base.JiyiApplication;
import com.jiyi.cradleheadcamera.common.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CameraSettingFlashFragment extends Fragment implements View.OnClickListener {


    @Bind(R.id.fragment_flash_off)
    RelativeLayout fragmentFlashOff;
    @Bind(R.id.fragment_flash_on)
    RelativeLayout fragmentFlashOn;
    @Bind(R.id.fragment_flash_auto)
    RelativeLayout fragmentFlashAuto;
    @Bind(R.id.fragment_flash_torch)
    RelativeLayout fragmentFlashTorch;
    @Bind(R.id.fragment_flash_layout)
    LinearLayout fragmentFlashLayout;

    public CameraSettingFlashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_camera_setting_flash, container, false);
        ButterKnife.bind(this, view);
        RxBus.get().register(this);
        fragmentFlashOff.setOnClickListener(this);
        fragmentFlashOn.setOnClickListener(this);
        fragmentFlashAuto.setOnClickListener(this);
        fragmentFlashTorch.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        int id = JiyiApplication.setting.get("flash");
        select(id);
    }
    public void select(int id) {
        for (int i = 0; i < fragmentFlashLayout.getChildCount(); i++) {
            View v = fragmentFlashLayout.getChildAt(i);
            if (v instanceof RelativeLayout) {
                for (int j = 0; j < ((RelativeLayout) v).getChildCount(); j++) {
                    View v2 = ((RelativeLayout) v).getChildAt(j);
                    if (v2 instanceof ImageView) {
                        if (v.getId() == id) {
                            v2.setVisibility(View.VISIBLE);
                        } else {
                            v2.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        RxBus.get().unregister(this);
    }

    @Override
    public void onClick(View v) {
        JiyiApplication.setting.put("flash",v.getId());
        switch (v.getId()) {
            case R.id.fragment_flash_off:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_CAMERA_FLASH, Camera.Parameters.FLASH_MODE_OFF);
                break;
            case R.id.fragment_flash_on:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_CAMERA_FLASH, Camera.Parameters.FLASH_MODE_ON);
                break;
            case R.id.fragment_flash_auto:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_CAMERA_FLASH, Camera.Parameters.FLASH_MODE_AUTO);
                break;
            case R.id.fragment_flash_torch:
                RxBus.get().post(Constants.TAG_CAMERA_CHANGE_CAMERA_FLASH, Camera.Parameters.FLASH_MODE_TORCH);
                break;
        }
        select(v.getId());
    }
}
