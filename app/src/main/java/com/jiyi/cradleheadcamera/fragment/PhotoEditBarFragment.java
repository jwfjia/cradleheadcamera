package com.jiyi.cradleheadcamera.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hwangjr.rxbus.RxBus;
import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.common.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotoEditBarFragment extends Fragment implements View.OnClickListener {


    @Bind(R.id.photo_edit_color)
    ImageView photoEditColor;
    @Bind(R.id.photo_edit_filter)
    ImageView photoEditFilter;

    public PhotoEditBarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo_edit_bar, container, false);
        ButterKnife.bind(this, view);
        RxBus.get().register(this);
        initListener();
        return view;
    }

    private void initListener() {
        photoEditColor.setOnClickListener(this);
        photoEditFilter.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        RxBus.get().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photo_edit_color:
                RxBus.get().post(Constants.TAG_PHOTO_EDIT_COLOR, 1);
                break;
            case R.id.photo_edit_filter:
                RxBus.get().post(Constants.TAG_PHOTO_EDIT_FILTER, 1);
                break;
        }
    }
}
