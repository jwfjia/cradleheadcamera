package com.jiyi.cradleheadcamera.bean;

/**
 * Created by IDesigner on 2017/5/16.
 */

public class DeviceStatus {
    String firmwareVersion, workingStatus, workingMode,proximity;

    public DeviceStatus(String firmwareVersion, String workingStatus, String workingMode, String proximity) {
        this.firmwareVersion = firmwareVersion;
        this.workingStatus = workingStatus;
        this.workingMode = workingMode;
        this.proximity = proximity;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public String getWorkingStatus() {
        return workingStatus;
    }

    public void setWorkingStatus(String workingStatus) {
        this.workingStatus = workingStatus;
    }

    public String getWorkingMode() {
        return workingMode;
    }

    public void setWorkingMode(String workingMode) {
        this.workingMode = workingMode;
    }

    public String getProximity() {
        return proximity;
    }

    public void setProximity(String proximity) {
        this.proximity = proximity;
    }
}
