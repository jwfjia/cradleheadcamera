package com.jiyi.cradleheadcamera.bean;

import java.util.concurrent.CountDownLatch;

/**
 * Created by IDesigner on 2017/6/5.
 */

public class MsgBody {
    CountDownLatch latch;
    byte[] data;

    public MsgBody(CountDownLatch latch, byte[] data) {
        this.latch = latch;
        this.data = data;
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
