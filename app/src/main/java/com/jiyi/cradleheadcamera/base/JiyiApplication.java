package com.jiyi.cradleheadcamera.base;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;

import com.jiyi.cradleheadcamera.R;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.service.BleService;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IDesigner on 2017/4/18.
 */

public class JiyiApplication extends Application {
    public static Map<String, Integer> setting = new HashMap<>();
    public static List<Camera.Size> frontCameraPictureSizeList;
    public static List<Camera.Size> backCameraPictureSizeList;
    public static List<Camera.Size> frontCameraVideoSizeList;
    public static List<Camera.Size> backCameraVideoSizeList;
    private static Context context;
    public static String isoValues;
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        initFileDir();
        initSetting();
    }
    public static Context getContextObject() {
        return context;
    }

    private void initSetting() {
        setting.put("whitebalance", R.id.fragment_white_balance_auto);
        setting.put("flash", R.id.fragment_flash_off);
        setting.put("grid", R.id.fragment_grid_none);
    }

    private void initFileDir() {
        File fileDir = new File(Constants.JIYI_PHOTO_DIRECTORY);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        File fileDir2 = new File(Constants.JIYI_VIDEO_DIRECTORY);
        if (!fileDir2.exists()) {
            fileDir2.mkdirs();
        }
    }
}
