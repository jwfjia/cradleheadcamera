package com.jiyi.cradleheadcamera.service;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

import com.clj.fastble.BleManager;
import com.clj.fastble.conn.BleCharacterCallback;
import com.clj.fastble.conn.BleGattCallback;
import com.clj.fastble.exception.BleException;
import com.clj.fastble.scan.ListScanCallback;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.jiyi.cradleheadcamera.bean.DeviceStatus;
import com.jiyi.cradleheadcamera.common.Constants;
import com.jiyi.cradleheadcamera.util.Codeutil;
import com.jiyi.cradleheadcamera.util.CommandDecoder;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.bluetooth.BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT;
import static android.bluetooth.BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE;


/**
 * Created by IDesigner on 2017/4/18.
 */

public class BleService extends Service {
    byte[] tempData = {1};
    BleManager bleManager;
    public BluetoothGatt tempGatt;
    public static final String TAG_REQUEST_SERVICE = "TAG_REQUEST_SERVICE";
    public static final String TAG_SEND_SERVICE = "TAG_SEND_SERVICE";
    byte[] result;
    CountDownLatch writeLatch;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RxBus.get().register(this);
        Log.i("lbw", "===service create");
        bleManager = new BleManager(getApplicationContext());
        RxBus.get().post(TAG_SEND_SERVICE, getBleService());
        if (!bleManager.isSupportBle()) {
            stopSelf();
        }
        bleManager.enableBluetooth();
//        scanBleDevice();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        flags = Service.START_FLAG_RETRY;
        return super.onStartCommand(intent, flags, startId);
    }

    public BleService getBleService() {
        return this;
    }

    public void scanBleDevice() {
        if (bleManager != null) {
            bleManager.scanDevice(new ListScanCallback(4000) {
                @Override
                public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                    super.onLeScan(device, rssi, scanRecord);
                    RxBus.get().post(Constants.TAG_SCAN, device);
                }

                @Override
                public void onDeviceFound(BluetoothDevice[] devices) {

                }
            });
        }

    }

    public void connectBleDevice(BluetoothDevice device) {
        Log.i("lbw", "===connectBleDevice");
//        if (device.getBondState() == BluetoothDevice.BOND_NONE) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                Log.i("lbw", "===createBond");
//                device.createBond();
//                bleManager.connectDevice(device, false, myBleGattCallback);
//            }
//        } else if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
//            Log.i("lbw", "===no Bond");
//            bleManager.connectDevice(device, false, myBleGattCallback);
//        }
        bleManager.connectDevice(device, false, myBleGattCallback);
    }


    public void startWrite(String serviceUUID, String characterUUID, byte[] data) {
        Log.i("lbw", "===write:" + Codeutil.bytesToHexString(data));
        boolean success = bleManager.writeDevice(serviceUUID, characterUUID, data, new BleCharacterCallback() {
            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {

            }

            @Override
            public void onFailure(BleException exception) {
                Log.d("lbw1", "===write fail:" + exception.getCode());
                bleManager.handleException(exception);
            }
        });

    }

    public void startWrite4Update(String serviceUUID, String characterUUID, byte[] data,CountDownLatch latch) {
        Log.i("lbw", "===write:" + Codeutil.bytesToHexString(data));
        writeLatch =latch;
        boolean success = bleManager.writeDevice(serviceUUID, characterUUID, data, new BleCharacterCallback() {
            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {

            }

            @Override
            public void onFailure(BleException exception) {
                Log.d("lbw1", "===write fail:" + exception.getCode());
                bleManager.handleException(exception);
            }
        });

    }

    public byte[] startWriteAndResponse(byte[] data) {
        result = null;
        final CountDownLatch latch = new CountDownLatch(1);
        Log.i("lbw", "===write2:" + Codeutil.bytesToHexString(data));
        bleManager.writeDevice("98d076f0-d6de-11e6-bf26-cec0c932ce01", "98d076f1-d6de-11e6-bf26-cec0c932ce01", data, new BleCharacterCallback() {
            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {
                Log.i("lbw", "===write2 success");
            }

            @Override
            public void onFailure(BleException exception) {
                Log.i("lbw", "===write2 fail" + exception.getDescription());
            }
        });

        bleManager.notify("98d076f0-d6de-11e6-bf26-cec0c932ce01", "98d076f2-d6de-11e6-bf26-cec0c932ce01", new BleCharacterCallback() {
            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {
                Log.i("lbw", "===notify2_suc:" + Codeutil.bytesToHexString(characteristic.getValue()));
                result = characteristic.getValue();
                latch.countDown();

            }

            @Override
            public void onFailure(BleException exception) {

            }
        });

        try {
            latch.await(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void startRead(String serviceUUID, String characterUUID) {
        boolean success = bleManager.readDevice(serviceUUID, characterUUID, new BleCharacterCallback() {
            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {
                Log.i("lbw", "===read:" + Codeutil.bytesToHexString(characteristic.getValue()));
            }

            @Override
            public void onFailure(BleException exception) {
                bleManager.handleException(exception);
            }
        });
    }

    public void startIndicate(String serviceUUID, String characterUUID) {
        boolean success = bleManager.indicate(serviceUUID, characterUUID, new BleCharacterCallback() {
            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {
                RxBus.get().post(Constants.TAG_INDICATE, Codeutil.bytesToHexString(characteristic.getValue()));
            }

            @Override
            public void onFailure(BleException exception) {
                bleManager.handleException(exception);
            }
        });
    }

    public void stopIndicate(String serviceUUID, String characterUUID) {
        bleManager.stopIndicate(serviceUUID, characterUUID);
    }

    public void startNotify(String serviceUUID, String characterUUID) {

        Log.i("lbw", "===startNotify");
        boolean success = bleManager.notify(serviceUUID, characterUUID, new BleCharacterCallback() {
            @Override
            public void onSuccess(BluetoothGattCharacteristic characteristic) {
//                Log.d("", "===test:notify success:" + Codeutil.bytesToHexString(characteristic.getValue()));
//                Log.i("lbw", "===notify_suc:" + Codeutil.bytesToHexString(characteristic.getValue()));
                if (characteristic.getValue()[0] == 0x55) {
                    try {
                        switch (characteristic.getValue()[1]) {
                            case (byte) 0xAA:
                                DeviceStatus status = CommandDecoder.parseStatus(characteristic.getValue());
                                RxBus.get().post(Constants.TAG_DEVICE_STATUS, status);
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                String cmd = Codeutil.bytesToHexString(characteristic.getValue());
                switch (cmd) {
                    case "55AB050100000001":
                        RxBus.get().post(Constants.TAG_DIALOG_CAMERA_SHUTTER, "");
                        break;
                    case "55AB050200000002":
                        RxBus.get().post(Constants.TAG_DIALOG_CAMERA_RECORD, "");
                        break;
                    case "55AB050001000001":
                        RxBus.get().post(Constants.TAG_DIALOG_CAMERA_ZOOM, true);
                        break;
                    case "55AB050002000002":
                        RxBus.get().post(Constants.TAG_DIALOG_CAMERA_ZOOM, false);
                        break;
                }

            }

            @Override
            public void onFailure(BleException exception) {
                Log.i("lbw", "===notify_fail:" + exception.getDescription());
                bleManager.handleException(exception);
            }
        });
        Log.i("lbw", "===startNotify:" + success);
    }


    public void stopNotify(String serviceUUID, String characterUUID) {
        bleManager.stopNotify(serviceUUID, characterUUID);
    }


    public void stopListen(String characterUUID) {
        bleManager.stopListenCharacterCallback(characterUUID);
    }

    public void stopConnect() {
        bleManager.closeBluetoothGatt();
        bleManager.stopListenConnectCallback();
    }

    public void closeBleGatt() {
        bleManager.closeBluetoothGatt();
    }

    BleGattCallback myBleGattCallback = new BleGattCallback() {
        @Override
        public void onNotFoundDevice() {
            Log.i("lbw", "===onNotFoundDevice");
        }

        @Override
        public void onFoundDevice(BluetoothDevice device) {
            Log.i("lbw", "===onFoundDevice");
        }

        @Override
        public void onConnectSuccess(BluetoothGatt gatt, int status) {
            Log.i("lbw", "===onConnectSuccess");
            gatt.discoverServices();
        }

        @Override
        public void onConnectFailure(BleException exception) {
            Log.i("lbw", "===onConnectFailure");
            bleManager.handleException(exception);
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            Log.i("lbw", "===onServicesDiscovered");
            tempGatt = gatt;
            RxBus.get().post(Constants.TAG_SERVICE_DISCOVERED, gatt);


        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i("lbw", "===蓝牙已断开");
            }

        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
//            Log.i("lbw", "===write result:" + Codeutil.bytesToHexString(characteristic.getValue()));
            if (writeLatch!=null)
                writeLatch.countDown();
        }
    };


    @Override
    public void onDestroy() {
        Log.i("lbw", "===serive onDestroy");
        super.onDestroy();
        RxBus.get().unregister(this);
        bleManager.closeBluetoothGatt();
        bleManager.stopListenConnectCallback();
        bleManager = null;

    }

    public void setWriteMode(boolean b) {
        if (tempGatt == null) {
            return;
        }
        UUID su = UUID.fromString("98d076f0-d6de-11e6-bf26-cec0c932ce01");
        UUID cu = UUID.fromString("98d076f1-d6de-11e6-bf26-cec0c932ce01");
        BluetoothGattService s1 = tempGatt.getService(su);
        BluetoothGattCharacteristic c1 = s1.getCharacteristic(cu);
        if (b) {
            if (c1 != null) {
                c1.setWriteType(WRITE_TYPE_DEFAULT);
            }
        } else {
            if (c1 != null) {
                c1.setWriteType(WRITE_TYPE_NO_RESPONSE);
            }
        }
    }

    public void clean() {
        bleManager.refreshDeviceCache();
    }

    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(BleService.TAG_REQUEST_SERVICE)})
    public void event(String i) {
        Log.i("lbw", "===TAG_REQUEST_SERVICE");
        RxBus.get().post(TAG_SEND_SERVICE, getBleService());
    }


    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_WRITE)})
    public void event2(byte[] bytes) {
        startWrite("98d076f0-d6de-11e6-bf26-cec0c932ce01", "98d076f1-d6de-11e6-bf26-cec0c932ce01", bytes);
    }


    @Subscribe(thread = EventThread.MAIN_THREAD, tags = {@Tag(Constants.TAG_READ)})
    public void event3(byte[] bytes) {
        startRead("98d076f0-d6de-11e6-bf26-cec0c932ce01", "98d076f2-d6de-11e6-bf26-cec0c932ce01");
    }


}
