package com.jiyi.cradleheadcamera.listener;

/**
 * Created by Administrator on 2017/4/10.
 */

public interface OnColorSeekbarChangeListener {
    void onColorChange(int type, float progress);
}
