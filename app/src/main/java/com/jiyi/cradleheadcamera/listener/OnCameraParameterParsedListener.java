package com.jiyi.cradleheadcamera.listener;

import java.util.List;

/**
 * Created by IDesigner on 2017/5/18.
 */

public interface OnCameraParameterParsedListener {
    void onPictureSizeParsed(List<String> list);

    void onVideoSizeParsed(List<String> list);

    void onIsoParsed(List<String> list);

}
