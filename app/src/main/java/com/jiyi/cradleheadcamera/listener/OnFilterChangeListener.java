package com.jiyi.cradleheadcamera.listener;

/**
 * Created by Administrator on 2017/4/11.
 */

public interface OnFilterChangeListener {
    void onFilterChange(int type, float progress);
}
