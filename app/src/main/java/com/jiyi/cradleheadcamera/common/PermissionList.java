package com.jiyi.cradleheadcamera.common;

import android.Manifest;

/**
 * Created by IDesigner on 2017/4/26.
 */

public class PermissionList {
    public static String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
}
