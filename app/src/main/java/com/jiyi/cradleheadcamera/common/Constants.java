package com.jiyi.cradleheadcamera.common;

import android.os.Environment;

/**
 * Created by IDesigner on 2017/4/18.
 */

public class Constants {
    public static final String JIYI_VIDEO_DIRECTORY = Environment.getExternalStorageDirectory() + "/jiyi/video/";
    public static final String JIYI_PHOTO_DIRECTORY = Environment.getExternalStorageDirectory() + "/jiyi/photo/";
    public static final String TAG_ = "";
    public static final String TAG_CAMERA_PICTURE_SIZE_CHANGE = "TAG_CAMERA_PICTURE_SIZE_CHANGE";
    public static final String TAG_CAMERA_VIDEO_SIZE_CHANGE = "TAG_CAMERA_VIDEO_SIZE_CHANGE";
    public static final String TAG_PANO_MODE = "TAG_PANO_MODE";
    public static final String TAG_PANO_READY = "TAG_PANO_READY";
    public static final String TAG_PANO_START = "TAG_PANO_START";
    public static final String TAG_PANO_END = "TAG_PANO_END";
    public static final String TAG_WRITE = "TAG_WRITE";
    public static final String TAG_DEVICE_STATUS = "TAG_DEVICE_STATUS";
    public static final String TAG_READ = "TAG_READ";
    public static final String TAG_NOTIFY = "TAG_NOTIFY";
    public static final String TAG_INDICATE = "TAG_INDICATE";
    public static final String TAG_SERVICE_DISCOVERED = "TAG_SERVICE_DISCOVERED";
    public static final String TAG_SCAN = "TAG_SCAN";
    public static final String TAG_RXBUS_REQUEST_SERVICE = "TAG_RXBUS_REQUEST_SERVICE";
    public static final String TAG_RXBUS_GET_SERVICE = "TAG_RXBUS_GET_SERVICE";

    public static final String TAG_DIALOG_CAMERA_SETTING = "TAG_DIALOG_CAMERA_SETTING";
    public static final String TAG_DIALOG_CRADLE_SETTING = "TAG_DIALOG_CRADLE_SETTING";
    public static final String TAG_DIALOG_SETTING = "TAG_DIALOG_SETTING";
    public static final String TAG_DIALOG_HOME = "TAG_DIALOG_HOME";
    public static final String TAG_DIALOG_PHOTO_DISPLAY = "TAG_DIALOG_PHOTO_DISPLAY";
    public static final String TAG_DIALOG_PHOTO_MODE = "TAG_DIALOG_PHOTO_MODE";
    public static final String TAG_DIALOG_CAMERA_SWITCH = "TAG_DIALOG_CAMERA_SWITCH";
    public static final String TAG_DIALOG_CAMERA_SHUTTER = "TAG_DIALOG_CAMERA_SHUTTER";
    public static final String TAG_DIALOG_CAMERA_RECORD = "TAG_DIALOG_CAMERA_RECORD";
    public static final String TAG_DIALOG_CAMERA_ZOOM = "TAG_DIALOG_CAMERA_ZOOM";
    public static final String TAG_FRAGMENT_CAMERA_SETTING = "TAG_FRAGMENT_CAMERA_SETTING";
    public static final String TAG_FRAGMENT_SHUTTER_SETTING_CHANGE = "TAG_FRAGMENT_SHUTTER_SETTING_CHANGE";

    public static final String TAG_CAMERA_CHANGE_WHITE_BALANCE = "TAG_CAMERA_CHANGE_WHITE_BALANCE";
    public static final String TAG_CAMERA_CHANGE_CAMERA_GRID = "TAG_CAMERA_CHANGE_CAMERA_GRID";
    public static final String TAG_CAMERA_CHANGE_CAMERA_FLASH = "TAG_CAMERA_CHANGE_CAMERA_FLASH";
    public static final String TAG_CAMERA_SHUTTER_DELAY = "TAG_CAMERA_SHUTTER_DELAY";
    public static final String TAG_PHOTO_PICK_PATH = "TAG_PHOTO_PICK_PATH";
    public static final String TAG_VIDEO_PICK_PATH = "TAG_VIDEO_PICK_PATH";
    public static final String TAG_PHOTO_EDIT_BAR = "TAG_PHOTO_EDIT_BAR";
    public static final String TAG_PHOTO_EDIT_COLOR = "TAG_PHOTO_EDIT_COLOR";
    public static final String TAG_PHOTO_EDIT_FILTER = "TAG_PHOTO_EDIT_FILTER";
    public static final String TAG_DIALOG_RECORD_TIME_LAPSE_MODE = "TAG_DIALOG_RECORD_TIME_LAPSE_MODE";
    public static final String TAG_DIALOG_RECORD_TIME_LAPSE_VALUE = "TAG_DIALOG_RECORD_TIME_LAPSE_VALUE";
    public static final String TAG_DIALOG_RECORD_TIME_LAPSE_MAX = "TAG_DIALOG_RECORD_TIME_LAPSE_MAX";
    public static final String TAG_START_TIME_COUNT = "TAG_START_TIME_COUNT";
    public static final String TAG_TRACKING_PARA = "TAG_TRACKING_PARA";

    public static final String TAG_PANO9 = "TAG_PANO9";
}
