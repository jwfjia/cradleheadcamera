package com.jiyi.cradleheadcamera.common;

/**
 * Created by IDesigner on 2017/5/16.
 */

public class Command {
    //陀螺仪校准
    public static final byte[] calibration_gyroscope = {(byte) 0xAA, 0x59, 0x02, 0x02, 0x02};
    //加速计校准
    public static final byte[] calibration_accelerometer = {(byte) 0xAA, 0x59, 0x02, 0x03, 0x03};
    //请求设备状态数据
    public static final byte[] request_device_status = {(byte) 0xAA, 0x55, 0x02, 0x11, 0x11};
    //设置全景模式180度
    public static final byte[] request_pano180_high = {(byte) 0xAA, 0x58, 0x03, 0x04, 0x02, 0x06};
    //设置全景模式320度
    public static final byte[] request_pano320_high = {(byte) 0xAA, 0x58, 0x03, 0x04, 0x0C, 0x10};
    //询问全景准备状态
    public static final byte[] request_pano_isready = {(byte) 0xAA, 0x56, 0x02, 0x01, 0x01};
    //全景开始
    public static final byte[] request_pano_start = {(byte) 0xAA, 0x56, 0x02, 0x02, 0x02};
    //全景结束
    public static final byte[] request_pano_end = {(byte) 0xAA, 0x56, 0x02, 0x03, 0x03};

    public static final byte[] rightleft = {(byte) 0xAA, 0x58, 0x03, 0x05, 0x02, 0x07};

    public static final byte[] tracking_start = {(byte) 0xAA, 0x57, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
}
